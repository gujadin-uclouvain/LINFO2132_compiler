package compiler.Semantic;

import compiler.Lexer.Symbols.Symbol;
import compiler.Parser.Negotiator;
import compiler.Parser.Structure.*;
import compiler.Tools.Context.Context;
import compiler.Semantic.Exception.*;
import compiler.Tools.Context.Structure.*;
import compiler.Tools.Type.*;

import java.util.*;

public class Analyser extends Checker implements ExpressionSemanticVisitor {
    @Override
    public Object visitArity0(Literal expression, Context context) {
        Symbol symbol = expression.getSymbol();
        if (isSimilar(symbol, Negotiator.VALUE)) {
            return castSymbolValue(symbol);

        } if (isSimilar(symbol, Negotiator.TYPE)) {
            return typeStringToObject(symbol.getName(), context);

        } if (isSimilar(symbol, Negotiator.IDENTIFIER)) {
            VariableData identifierData = context.get(symbol.getToken());
            if (identifierData == null) throw new RuntimeException("Not declared identifier "+symbol.getToken());
            return new Variable(symbol.getToken(), identifierData);

        }
        throw new SemanticUnknownSymbolException(symbol);
    }

    @Override
    public Object visitArity1(Unary expression, Context context) {
        Symbol symbol = expression.getSymbol();
        Object right = expression.getChild(0).checkSemantic(this, context);
        if (isSimilar(symbol, Negotiator.IDENTIFIER)) { // With Type
            VariableData identifierData;
            if (right instanceof Variable v) {
                identifierData = new VariableData(typeObjectToType(context.get(v.getIdentifier()).getType()), null, null);
            } else identifierData = new VariableData(typeObjectToType(right), null, null);
            context.add(symbol.getToken(), identifierData, true);
            return new Variable(symbol.getToken(), identifierData);

        } if (isSimilar(symbol, Negotiator.DECLARATOR)) {
            if (right instanceof Variable vRight) {
                context.checkAndSetDeclarator(vRight.getIdentifier(), symbol.getName());
                vRight.getData().setDeclarator(symbol.getName());
                return right;
            } throw new RuntimeException("Declarator Error: Missing identifier with declarator "+symbol.getName());

        } if (isSimilar(symbol, Negotiator.RETURN)) {
            BasicType returnType = context.get("|RETURN|").getType();
            if (right instanceof Variable vRight) {
                return checkSameType(
                        typeStringToObject(returnType.getTypeID(), context),
                        typeStringToObject(vRight.getData().getType().getTypeID(), context)
                );
            }
            return checkSameType(typeStringToObject(returnType.getTypeID(), context), right);

        } if (isSimilar(symbol, Negotiator.FOR_KEYWORD)) {
            if (isIntegerType(right)) return true;
            throw new RuntimeException(symbol.getName()+" must be followed by an Integer");

        } if (isSimilar(symbol, Negotiator.ARRAY)) { // Declaration OR Constructor
            return new ArrayType(typeObjectToString(right));

        } return null;
    }

    private void addRecordProcedureIdentifierInContext(Binary expression, Context context) {
        if (isSimilar(expression.getSymbol(), new Negotiator[]{Negotiator.RECORD, Negotiator.PROC_INITIATOR})) {
            Symbol record = expression.getChild(0).getSymbol();
            if (isSimilar(expression.getSymbol(), Negotiator.RECORD)) { // Is Record Type
                context.add(record.getToken(), new VariableData(new RecordType(record.getToken())));
                context.checkAndSetDeclarator(record.getToken(), "RECORD");
            } else {                                    // Is Procedure Type
                context.add(record.getToken(), new VariableData(new ProcedureType(record.getToken())));
                context.checkAndSetDeclarator(record.getToken(), "PROCEDURE");
            }
        }
    }

    @Override
    public Object visitArity2(Binary expression, Context context) {
        Symbol symbol = expression.getSymbol();
        addRecordProcedureIdentifierInContext(expression, context);
        Object left = expression.getChild(0).checkSemantic(this, context);
        if (isSimilar(symbol, Negotiator.RECORD_FIELD_ACCESS)) { // Access record field (must be already declared)
            if (left instanceof Variable vLeft) {
                String key = vLeft.getIdentifier();
                if (context.get(key) == null) throw new RuntimeException("Not declared record identifier "+key);
                VariableData data = context.get(key);
                Symbol fieldSymbol = expression.getChild(1).getSymbol();
                if (!(data.getType() instanceof RecordType recordType)) throw new RuntimeException("Identifier "+key+" is not a record");
                if (recordType.get(fieldSymbol.getToken()) == null) throw new RuntimeException("Field "+fieldSymbol.getToken()+" doesn't exist in record "+recordType);
                if (recordType.get(fieldSymbol.getToken()) instanceof RecordType subRecordType)
                    return new Variable(subRecordType.getTypeID(), context.get(subRecordType.getTypeID()));
                else return typeStringToObject(recordType.get(fieldSymbol.getToken()).getTypeID(), context);
            }
            throw new RuntimeException("Identifier for record field access is not valid: "+left);
        }
        Object right = expression.getChild(1).checkSemantic(this, context);

        if (isSimilar(symbol, new Negotiator[]{Negotiator.WEAK_OPERATOR, Negotiator.STRONG_OPERATOR, Negotiator.LOGICAL, Negotiator.COMPARATOR})) {
            return applyOperation(left, right, symbol.getToken());

        } if (isSimilar(symbol, new Negotiator[]{Negotiator.ASSIGN})) {
            return checkAssign(left, right, context);

        } if (isSimilar(symbol, new Negotiator[]{Negotiator.IF, Negotiator.WHILE, Negotiator.FOR})) {
            return isBooleanType(left) && right != null;

        } if (isSimilar(symbol, Negotiator.ARRAY)) { // Access index (must be already declared)
            if (left instanceof Variable vLeft) {
                String key = vLeft.getIdentifier();
                if (context.get(key) == null) throw new RuntimeException("Not declared array identifier "+key);
                if (!isIntegerType(right)) throw new RuntimeException("Array access must be an Integer");
                VariableData data = context.get(key);
                if (!(data.getType() instanceof ArrayType arrayType)) throw new RuntimeException("Identifier "+key+" is not an array");
                Integer iRight = (Integer) right;
                if (!(arrayType.length() > iRight && iRight >= 0)) throw new IndexOutOfBoundsException(iRight+" must be between [0, "+(arrayType.length()-1)+"]");
                return left;
            }

        } if (isSimilar(symbol, Negotiator.INITIATOR)) {
            if (left instanceof ArrayType arrayTypeLeft) { // Array constructor
                if (!(right instanceof ArgumentsContainer aRight)) throw new RuntimeException("Array constructor must be initialized by an Integer");
                arrayTypeLeft.setLength((Integer) aRight.get(0));
                return arrayTypeLeft;

            } else if (left instanceof Variable vLeft) { // record constructor or record call or procedure call
                String name = vLeft.getIdentifier();
                VariableData recordProc = context.get(name);
                if (right instanceof ArgumentsContainer aRight && recordProc.getType() instanceof RecordType types) {
                    Object[] args = Arrays.stream(aRight.getContent()).map(this::typeObjectToType).toArray();
                    if (types.compare(args)) return new Variable(name, recordProc);
                }
            }

        } if (isSimilar(symbol, new Negotiator[]{Negotiator.RECORD, Negotiator.PROC_INITIATOR})) {
            if (left instanceof Variable vLeft) {
                String name = vLeft.getIdentifier();
                VariableData record = context.get(name);
                if (record.getType() instanceof RecordType r) {
                    if (right instanceof FieldsContainer container) r.addAll(container);
                    if (right instanceof ArgumentsContainer container) r.addAll(container);
                    return new Variable(name, record);
                }
            }

        } return null;
    }

    @Override
    public Object visitArity3(Ternary expression, Context context) {
        Symbol symbol = expression.getSymbol();
        Object left = expression.getChild(0).checkSemantic(this, context);
        Object middle = expression.getChild(1).checkSemantic(this, context);
        if (isSimilar(symbol, new Negotiator[]{Negotiator.PROCEDURE})) {
            if (left instanceof Variable proc && proc.getData().getType() instanceof ProcedureType type) {
                type.addReturnType(typeObjectToType(middle));
                return null;
            }
        }
        Object right = expression.getChild(2).checkSemantic(this, context);
        if (isSimilar(symbol, new Negotiator[]{Negotiator.IF})) {
            return isBooleanType(left) && middle != null && right != null;

        } if (isSimilar(symbol, new Negotiator[]{Negotiator.FOR_SEQUENCE})) {
            return isIntegerType(left);

        } return null;
    }

    @Override
    public Object visitArityN(Block expression, Context context) {
        Context nextContext;
        if (expression.getSymbol().equalsName("BLOCK")) nextContext = context;
        else if (expression.getSymbol().equalsName("BODY_SHADOW")) {
            nextContext = new Context(context, true);
            ProcedureType procType = (ProcedureType) context.getLast().getType();
            procType.addArgsToContext(nextContext);
            nextContext.add("|RETURN|", new VariableData(procType.getReturnType(), null, "SYSTEM"));
        }
        else nextContext = new Context(context, false);

        FieldsContainer field = new FieldsContainer();
        ArgumentsContainer args = new ArgumentsContainer();

        Object result = null;
        for (Expression child : expression.getChildren()) {
            result = child.checkSemantic(this, nextContext);
            if (expression.getSymbol().equalsName("ARGUMENT")) {
                args.add(result);
            }
            else if (expression.getSymbol().equalsName("FIELD")) {
                if (result instanceof Variable vResult) {
                    if (!field.add(vResult)) throw new RuntimeException("Already declared field "+vResult.getIdentifier());
                } else throw new RuntimeException("Congratulation, You found the flag !!!");
            }
        }
        nextContext.intersect(context);
        if (expression.getSymbol().equalsName("ARGUMENT")) return args;
        if (expression.getSymbol().equalsName("FIELD")) return field;
        return result;
    }
}

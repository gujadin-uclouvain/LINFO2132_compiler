package compiler.Semantic;

import compiler.Lexer.Symbols.Symbol;
import compiler.Parser.Negotiator;
import compiler.Tools.Context.Context;
import compiler.Semantic.Exception.SemanticTokenParseException;
import compiler.Tools.Context.Structure.Variable;
import compiler.Tools.Context.Structure.VariableData;
import compiler.Tools.Type.*;

import static org.objectweb.asm.Opcodes.IF_ICMPEQ;
import static org.objectweb.asm.Opcodes.IF_ICMPNE;

public abstract class Checker {
    public final String integerTypeID = "INTEGER";
    public final String realTypeID = "REAL";
    public final String booleanTypeID = "BOOLEAN";
    public final String stringTypeID = "STRING";
    public final String voidTypeID = "VOID";

    public boolean isSimilar(Symbol symbol, Negotiator[] negotiators) {
        for (Negotiator negotiator : negotiators) {
            if (isSimilar(symbol, negotiator)) return true;
        }
        return false;
    }
    public boolean isSimilar(Symbol symbol, Negotiator negotiator) {
        for (String symbolName : negotiator.get()) {
            if (symbol.equalsName(symbolName)) return true;
        }
        return false;
    }

    public Object applyOperation(Object nbr1, Object nbr2, String tokenOperation) {
        if (nbr1 instanceof Variable v) {
            if (v.getData().getContent() == null) nbr1 = typeBaseStringToObject(v.getData().getType().getTypeID());
            else nbr1 = v.getData().getContent();
        }
        if (nbr2 instanceof Variable v) {
            if (v.getData().getContent() == null) nbr2 = typeBaseStringToObject(v.getData().getType().getTypeID());
            else nbr2 = v.getData().getContent();
        }

        if (nbr1 instanceof Float d1 && nbr2 instanceof Float d2) return applyOperation(d1, d2, tokenOperation);
        if (nbr1 instanceof Integer i1 && nbr2 instanceof Float d2) return applyOperation(Float.valueOf(i1), d2, tokenOperation);
        if (nbr1 instanceof Float d1 && nbr2 instanceof Integer i2) return applyOperation(d1, Float.valueOf(i2), tokenOperation);
        if (nbr1 instanceof Integer i1 && nbr2 instanceof Integer i2) return applyOperation(i1, i2, tokenOperation);
        if (nbr1 instanceof Boolean b1 && nbr2 instanceof Boolean b2) return applyOperation(b1, b2, tokenOperation);
        return null;
    }
    public Object applyOperation(Float nbr1, Float nbr2, String tokenOperation) {
        return switch (tokenOperation) {
            case "+" -> nbr1 + nbr2;
            case "-" -> nbr1 - nbr2;
            case "*" -> nbr1 * nbr2;
            case "/" -> nbr1 / nbr2;
            case "%" -> nbr1 % nbr2;
            case "==" -> nbr1 == nbr2;
            case "<>" -> nbr1 != nbr2;
            case "<=" -> nbr1 <= nbr2;
            case "<"  -> nbr1 < nbr2;
            case ">=" -> nbr1 >= nbr2;
            case ">"  -> nbr1 > nbr2;
            default -> null;
        };
    }
     public Object applyOperation(Integer nbr1, Integer nbr2, String tokenOperation) {
        return switch (tokenOperation) {
            case "+" -> nbr1 + nbr2;
            case "-" -> nbr1 - nbr2;
            case "*" -> nbr1 * nbr2;
            case "/" -> nbr1 / nbr2;
            case "%" -> nbr1 % nbr2;
            case "==" -> nbr1 == nbr2;
            case "<>" -> nbr1 != nbr2;
            case "<=" -> nbr1 <= nbr2;
            case "<"  -> nbr1 < nbr2;
            case ">=" -> nbr1 >= nbr2;
            case ">"  -> nbr1 > nbr2;
            default -> null;
        };
    }
    public Object applyOperation(Boolean bool1, Boolean bool2, String tokenOperation) {
        return switch (tokenOperation) {
            case "and" -> bool1 && bool2;
            case "or" -> bool1 || bool2;
            case "==" -> bool1 == bool2;
            case "<>" -> bool1 != bool2;
            default -> null;
        };
    }

    public Object checkAssign(Object left, Object right, Context context) {
        if (left instanceof Variable vLeft && right instanceof Variable vRight) return checkAssign(vLeft, vRight, context);
        if (left instanceof Variable vLeft && right instanceof ArrayType aRight) return checkAssign(vLeft, aRight, context);
        if (left instanceof Variable vLeft) return checkAssign(vLeft, right, context);
        return checkSameType(left, right);
    }
    public Object checkAssign(Variable left, ArrayType right, Context context) {
        String key = left.getIdentifier();
        VariableData leftData = context.get(key);
        BasicType typeLeft = leftData.getType();
        if (typeLeft.equals(right)) {
            checkIsMutableIdentifier(key, leftData);
            Object ret = checkSameType(typeStringToObject(typeLeft.getTypeID(), context), typeStringToObject(right.getTypeID(), context));
            context.add(key, new VariableData(right, leftData.getContent(), leftData.getDeclarator()));
            return ret;
        }
        throw new RuntimeException("Assignation Error: Array constructor '"+left.getIdentifier()+"' cannot be assign to type "+right.getTypeID());
    }
    public Object checkAssign(Variable left, Variable right, Context context) {
        String keyLeft = left.getIdentifier();
        String keyRight = right.getIdentifier();
        VariableData leftData = context.get(keyLeft);
        VariableData rightData = context.get(keyRight);
        checkIsMutableIdentifier(keyLeft, leftData);
        BasicType typeLeft = leftData.getType();
        BasicType typeRight;
        if (rightData.getType() instanceof ProcedureType rightDataProc) {
            typeRight = rightDataProc.getReturnType();
        } else typeRight = rightData.getType();
        Object ret = checkSameType(typeStringToObject(typeLeft.getTypeID(), context), typeStringToObject(typeRight.getTypeID(), context));
        context.add(keyLeft, new VariableData(typeLeft, rightData.getContent(), leftData.getDeclarator()));
        return ret;
//        throw new RuntimeException("Assignation Error: Need both identifier");
    }
    public Object checkAssign(Variable left, Object right, Context context) {
        String key = left.getIdentifier();
        VariableData data = context.get(key);
        checkIsMutableIdentifier(key, data);
        BasicType type = data.getType();
        Object ret = checkSameType(typeStringToObject(type.getTypeID(), context), right);
        context.add(key, new VariableData(type, right, data.getDeclarator()));
        return ret;
//        throw new RuntimeException("AssignationError: "+left+" is not an identifier");
    }

    public void checkIsMutableIdentifier(String identifier, VariableData data) {
        if (!(data.getDeclarator().equals("MUTABLE") || data.getContent() == null))
            throw new RuntimeException("Identifier "+identifier+" is a(n) "+data.getDeclarator());
    }

//    public Object checkSameType(Object o1, Variable o2) { return checkSameType(o1, o2.getData().getType()) }
    public Object checkSameType(Object o1, Object o2) {
        if (isIntegerType(o1, o2)) return 1;
        else if (isRealType(o1, o2)) return 1.0F;
        else if (isBooleanType(o1, o2)) return true;
        else if (isStringType(o1, o2)) return "string";
        else if (isArrayType(o1, o2)) return o1;
        else if (isRecordType(o1, o2)) return o1;
        throw new RuntimeException(typeObjectToString(o1)+" is not same type as "+typeObjectToString(o2));
    }

    public boolean isIntegerType(Object o1, Object o2) { return isIntegerType(o1) && isIntegerType(o2); }
    public boolean isIntegerType(Object o) { return (o instanceof Integer); }
    public boolean isRealType(Object o1, Object o2) { return isRealType(o1) && isRealType(o2); }
    public boolean isRealType(Object o) { return (o instanceof Float); }
    public boolean isBooleanType(Object o1, Object o2) { return isBooleanType(o1) && isBooleanType(o2); }
    public boolean isBooleanType(Object o) { return (o instanceof Boolean); }
    public boolean isStringType(Object o1, Object o2) { return isStringType(o1) && isStringType(o2); }
    public boolean isStringType(Object o) { return (o instanceof String); }
    public boolean isArrayType(Object o1, Object o2) { return (o1 instanceof ArrayType a1 && o2 instanceof ArrayType a2 && a1.equals(a2)); }
    public boolean isArrayType(Object o) { return (o instanceof ArrayType); }
    public boolean isRecordType(Object o1, Object o2) { return (o1 instanceof RecordType a1 && o2 instanceof RecordType a2 && a1.equals(a2)); }
    public boolean isRecordType(Object o) { return (o instanceof RecordType); }
    public boolean isVariableType(Object o) { return (o instanceof Variable); }

    public Integer setAsInteger(String token) { return Integer.valueOf(token); }
    public Float setAsFloat(String token) { return Float.valueOf(token); }
    public Boolean setAsBoolean(String token) { return token.equals("true"); }
    public String setAsString(String token) { return token.substring(1, token.length()-1); }

    public Object castSymbolValue(Symbol symbol) {
        switch (symbol.getName()) {
            case "INTEGER_VALUE":   return setAsInteger(symbol.getToken());
            case "REAL_VALUE":      return setAsFloat(symbol.getToken());
            case "BOOLEAN_VALUE":   return setAsBoolean(symbol.getToken());
            case "STRING_VALUE":    return setAsString(symbol.getToken());
        }
        throw new SemanticTokenParseException(symbol.getName());
    }
    public Object typeStringToObject(String name, Context context) {
        switch (name) {
            case integerTypeID:     return 1;
            case realTypeID:        return 1.0F;
            case booleanTypeID:     return true;
            case stringTypeID:      return "string";
            case voidTypeID:        return null;
        }
        if (context.get(name) != null) return context.get(name).getType();
        throw new SemanticTokenParseException(name);
    }
    public Object typeBaseStringToObject(String name) {
        switch (name) {
            case integerTypeID:     return 1;
            case realTypeID:        return 1.0F;
            case booleanTypeID:     return true;
            case stringTypeID:      return "string";
        }
        throw new SemanticTokenParseException(name);
    }
    public BasicType typeObjectToType(Object o) {
        if (isIntegerType(o))   return new BasicType(integerTypeID);
        if (isRealType(o))      return new BasicType(realTypeID);
        if (isBooleanType(o))   return new BasicType(booleanTypeID);
        if (isStringType(o))    return new BasicType(stringTypeID);
        if (o == null)          return new BasicType(voidTypeID);
        if (isArrayType(o))     return (ArrayType) o;
        if (isRecordType(o))    return (RecordType) o;
        if (isVariableType(o))  return ((Variable) o).getData().getType();
        throw new RuntimeException("Type Error: "+o+" is not valid type");
    }
    public String typeObjectToString(Object o) {
        if (isIntegerType(o))   return integerTypeID;
        if (isRealType(o))      return realTypeID;
        if (isBooleanType(o))   return booleanTypeID;
        if (isStringType(o))    return stringTypeID;
        if (isArrayType(o))     return o.toString();
        if (isRecordType(o))    return ((RecordType) o).getTypeID();
        if (isVariableType(o))  return ((Variable) o).getData().getType().getTypeID();
        throw new RuntimeException("Type Error: "+o+" is not valid type");
    }

    public void addBuiltInProcedureInContext(Context context) {
        context.addProcedure("not",
                new BasicType(booleanTypeID),
                new Variable("b", new VariableData(new BasicType(booleanTypeID)))
        );
        context.addProcedure("chr",
                new BasicType(stringTypeID),
                new Variable("i", new VariableData(new BasicType(integerTypeID)))
        );
        context.addProcedure("len",
                new BasicType(integerTypeID),
                new Variable("s", new VariableData(new BasicType(stringTypeID)))
        ); // TODO: Missing len with Arrays
        context.addProcedure("floor",
                new BasicType(realTypeID),
                new Variable("i", new VariableData(new BasicType(integerTypeID)))
        );
        // Write functions
        context.addProcedure("writeInt",
                new BasicType(voidTypeID),
                new Variable("i", new VariableData(new BasicType(integerTypeID)))
        );
        context.addProcedure("writeBool",
                new BasicType(voidTypeID),
                new Variable("b", new VariableData(new BasicType(booleanTypeID)))
        );
        context.addProcedure("writeReal",
                new BasicType(voidTypeID),
                new Variable("r", new VariableData(new BasicType(realTypeID)))
        );
        context.addProcedure("write",
                new BasicType(voidTypeID),
                new Variable("o", new VariableData(new BasicType(stringTypeID)))
        );
        context.addProcedure("writeln",
                new BasicType(voidTypeID),
                new Variable("o", new VariableData(new BasicType(stringTypeID)))
        );
        // Read functions
        context.addProcedure("readInt",
                new BasicType(integerTypeID)
        );
        context.addProcedure("readReal",
                new BasicType(realTypeID)
        );
        context.addProcedure("readString",
                new BasicType(stringTypeID)
        );
    }
}

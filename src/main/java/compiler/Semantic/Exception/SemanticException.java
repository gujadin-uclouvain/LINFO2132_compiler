package compiler.Semantic.Exception;

public abstract class SemanticException extends RuntimeException {
    public SemanticException() {}
    public SemanticException(String message) {
        super(message);
    }
}

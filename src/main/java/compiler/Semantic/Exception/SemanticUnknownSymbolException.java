package compiler.Semantic.Exception;

import compiler.Lexer.Symbols.Symbol;

public class SemanticUnknownSymbolException extends SemanticException {
    public SemanticUnknownSymbolException(Symbol symbol) {
        super("Symbol" + ((symbol == null) ? " " : "'" + symbol.getName() + "' ") + " is unknown");
    }
}

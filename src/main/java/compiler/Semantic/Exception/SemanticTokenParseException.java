package compiler.Semantic.Exception;

public class SemanticTokenParseException extends SemanticException {
    public SemanticTokenParseException(String token) {
        super("The token '"+token+"' cannot be parsed");
    }
}

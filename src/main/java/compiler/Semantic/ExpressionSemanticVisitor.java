package compiler.Semantic;

import compiler.Parser.Structure.*;
import compiler.Tools.Context.Context;

public interface ExpressionSemanticVisitor {
    /**
     * Use for the following Expressions Types (Thanks to Negotiator Singleton)
     *      *VALUE
     *      *TYPE
     *      *IDENTIFIER (without type) [NOT DEFINE]
     */
    Object visitArity0(Literal expression, Context context);

    /**
     * Use for the following Expressions Types (Thanks to Negotiator Singleton)
     *      *IDENTIFIER (with type)
     *      RETURN
     *      *FOR_KEYWORD
     *      *ARRAY (array constructor OR array type)
     */
    Object visitArity1(Unary expression, Context context);

    /**
     * Use for the following Expressions Types (Thanks to Negotiator Singleton)
     *      *ARRAY (index access)
     *      *ASSIGN
     *      *DECLARATOR
     *      *LOGICAL
     *      *COMPARATOR
     *      *WEAK_OPERATOR
     *      *STRONG_OPERATOR
     *      *RECORD_FIELD_ACCESS
     *      *IF (without else)
     *      *WHILE
     *      *FOR
     *      *RECORD
     *      INITIATOR (record & array constructor AND function calls)
     *      PROC_INITIATOR (Function initialization)
     */
    Object visitArity2(Binary expression, Context context);

    /**
     * Use for the following Expressions Types (Thanks to Negotiator Singleton)
     *      *IF (with else)
     *      PROCEDURE
     *      *FOR_SEQUENCE
     */
    Object visitArity3(Ternary expression, Context context);

    /**
     * Use for the following Expressions Types (Thanks to Negotiator Singleton)
     *      *BODY
     *      BODY_SHADOW
     *      ARGUMENT (for procedures and calls)
     *      *FIELD (for records)
     */
    Object visitArityN(Block expression, Context context);
}

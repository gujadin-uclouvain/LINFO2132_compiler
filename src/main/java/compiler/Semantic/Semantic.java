package compiler.Semantic;

import compiler.Parser.Structure.Expression;
import compiler.Tools.Context.Context;

public class Semantic {
    private final Analyser analyser = new Analyser();
    private final Context context = new Context();

    public boolean analyse(Expression current) {
        analyser.addBuiltInProcedureInContext(context);
        current.checkSemantic(analyser, context);
        return true;
    }
}

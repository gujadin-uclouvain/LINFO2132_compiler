package compiler.Lexer;
import compiler.Lexer.Exception.LexerNotValidTokenException;
import compiler.Lexer.Symbols.Symbol;
import compiler.Lexer.Symbols.SymbolType;

import java.io.IOException;
import java.io.Reader;

public class Lexer extends LexerTools {
    private final Reader input;
    private boolean ending;

    private Symbol lastSymbol;
    private Symbol lastCommentSymbol;
    private char lastChar;

    public Lexer(Reader input) {
        super();
        this.input = input;
        this.ending = false;
        this.lastChar = 0;
    }

    public boolean isEnding() { return this.ending; }

    private Symbol determineNextSymbol(char currChar, String lastChars) {
        Symbol nextSymbol = this.getDictionary().get(lastChars+currChar);           // Combine lastChars with currChar
        if (nextSymbol == null) nextSymbol = this.getDictionary().get(lastChars);   // Check lastChars
        if (nextSymbol == null) nextSymbol = this.getIdentifierOrValue(lastChars);  // Not Found => Identifier || Value
        return nextSymbol;
    }

    private Symbol getIdentifierOrValue(String expr) {
        if (this.isValue(expr)) {
            return this.getDictionary().createValue(expr);
        } if (this.isIdentifier(expr)) {
            return this.getDictionary().createIdentifier(expr);
        } else throw new LexerNotValidTokenException(expr);
    }

    private void saveLastChar(char currChar, StringBuilder stringBuilder, boolean isCombinationSequence) {
        if (!(this.isSeparator(currChar) || isCombinationSequence)) {
            this.lastChar = currChar;
            stringBuilder.delete(0, stringBuilder.length());
        }
    }

    private void saveLastSymbol(Symbol symbol) {
        if (symbol.getType() == SymbolType.COMMENT) this.lastCommentSymbol = symbol;
        else this.lastSymbol = symbol;
    }

    private void skipCommentValue() throws IOException {
        char currChar = this.getNextCharacter();
        while (currChar != '\n' && !this.isEnding()) {
            currChar = this.getNextCharacter();
        }
        this.lastCommentSymbol = null;
    }

    private char getNextCharacter() throws IOException {
        int redValue = this.input.read();
        if (redValue == -1) {
            this.ending = true;
            this.input.close();
            return 0;
        }
        return Character.toChars(redValue)[0];
    }

    public Symbol getNextSymbol() throws IOException {
        if (isEnding()) return null;

        StringBuilder symbolBuilder = new StringBuilder();
        Symbol nextSymbol = null;
        boolean hasFoundSymbol = false;
        boolean hasCheckNextChar = false;
        boolean isStringValue = false;

        // Char stored from previous fetch
        if (this.lastChar != 0) {
            symbolBuilder.append(this.lastChar); this.lastChar = 0;
        }

        // Check comments case
        if (lastCommentSymbol != null && lastCommentSymbol.getType() == SymbolType.COMMENT) {
            this.skipCommentValue();
        }

        while(!(hasFoundSymbol && hasCheckNextChar)) { // Double check
            // End of file
            if (this.isEnding()) {
                if (!symbolBuilder.isEmpty()) return this.getDictionary().get(symbolBuilder.toString());
                return null;
            }

            char currChar = this.getNextCharacter();

            // Check string case
            if (!isStringValue) isStringValue = this.isStringStart(symbolBuilder.toString(), currChar);

            // Char is a basic char
            if (this.isInCurrentSequence(currChar, symbolBuilder.toString()) || isStringValue) {
                symbolBuilder.append(Character.toChars(currChar));

                // Is not a String
                if (!this.isStringStart(symbolBuilder.toString(), currChar)) {
                    nextSymbol = this.getDictionary().get(symbolBuilder.toString());

                // Is the end of the String
                } else if (this.isStringFinished(symbolBuilder.toString(), currChar)) {
                    nextSymbol = this.getIdentifierOrValue(symbolBuilder.toString());
                    this.lastSymbol = nextSymbol;
                    return nextSymbol;
                }

            // Char is a separator || special character
            } else {
                boolean isCombinationSequence = this.isMultipleSpecialSymbols(currChar, symbolBuilder.toString());
                // No characters need to be analysed
                if (!symbolBuilder.isEmpty() || isCombinationSequence) {
                    String stringSymbol = symbolBuilder.toString();

                    // Not Separator && Not Symbol Combination => Store it for next symbol
                    this.saveLastChar(currChar, symbolBuilder, isCombinationSequence);

                    nextSymbol = this.determineNextSymbol(currChar, stringSymbol);
                    this.saveLastSymbol(nextSymbol);
                    if (this.lastCommentSymbol != null) return getNextSymbol();
                    else return nextSymbol;

                // Some characters need to be analysed => Just append the current one
                } else {
                    if (!this.isSeparator(currChar)) symbolBuilder.append(currChar);
                }
            }

            hasCheckNextChar = hasFoundSymbol;
            hasFoundSymbol = nextSymbol != null;
        }

        this.lastSymbol = nextSymbol;
        return nextSymbol;
    }
}

package compiler.Lexer.Exception;

public abstract class LexerException extends RuntimeException {
    public LexerException() {}
    public LexerException(String message) {
        super(message);
    }
}

package compiler.Lexer.Exception;

public class LexerNotValidTokenException extends LexerException {
    public LexerNotValidTokenException(String token) {
        super("The token '"+token+"' is not a valid token");
    }
}

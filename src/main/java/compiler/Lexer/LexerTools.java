package compiler.Lexer;

import compiler.Lexer.Symbols.Symbol;
import compiler.Lexer.Symbols.SymbolManager;
import compiler.Lexer.Symbols.SymbolType;

class LexerTools {
    private final SymbolManager dictionary;

    LexerTools() {
        this.dictionary = new SymbolManager();
    }

    SymbolManager getDictionary() {return this.dictionary;}

    boolean isSeparator(char input) { return input == ' ' || input == '\t' || input == '\n'; }

    boolean isIdentifier(String input) {
        char c = input.charAt(0);
        return (('a' <= c && c <='z') || ('A' <= c && c <='Z') || c == '_') &&
                !Character.isDigit(c) && !StringValue.isBoolean(input);
    }
    boolean isValue(String input) { return StringValue.isNumerical(input) || StringValue.isString(input) || StringValue.isChar(input) || StringValue.isBoolean(input); }

    boolean isSpecialType(Symbol symbol) {
        if (symbol != null) {
            return  symbol.equalsType(SymbolType.OPERATOR) ||
                    symbol.equalsType(SymbolType.COMPARATOR) ||
                    symbol.equalsType(SymbolType.ASSIGNMENT) ||
                    symbol.equalsType(SymbolType.SINGLE_CHARACTER);
        }
        return false;
    }

    boolean isSpecialCharacter(String input) { return this.isSpecialType(this.getDictionary().get(input)); }
    boolean isSpecialCharacter(char input) { return this.isSpecialCharacter(String.valueOf(input)); }

    boolean isInCurrentSequence(char currChar, String lastChars) {
        return !this.isSeparator(currChar)           &&
               !this.isSpecialCharacter(currChar)    &&
               !this.isSpecialCharacter(lastChars)   ||
                this.isFloatDot(lastChars, currChar);
    }

    boolean isMultipleSpecialSymbols(char currChar, String lastChars) {
        if (lastChars.isEmpty()) return false;
        return this.getDictionary().exists(lastChars+currChar);
    }

    boolean isFloatDot(String lastChars, char currentChar) {
        if (lastChars.isEmpty()) return false;
        return StringValue.isNumerical(lastChars) && currentChar == '.';
    }

    boolean isStringStart(String lastChars, char currentChar) {
        return (lastChars.isEmpty() && currentChar == '"') || (lastChars.startsWith("\""));
    }
    boolean isStringFinished(String lastChars, char currentChar) {
        return lastChars.length() > 1 && (currentChar == '"' || lastChars.endsWith("\""));
    }
}

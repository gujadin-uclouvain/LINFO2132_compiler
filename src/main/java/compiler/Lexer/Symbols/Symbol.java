package compiler.Lexer.Symbols;

import java.util.Objects;

public final class Symbol {
    private final String name;
    private final String token;
    private final SymbolType type;

    public Symbol(String name, String token, SymbolType type) {
        this.name = name;
        this.token = token;
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public String getToken() {
        return this.token;
    }

    public SymbolType getType() {
        return this.type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o instanceof String s) return this.equalsToken(s);
        if (!(o instanceof Symbol symbol)) return false;
        return this.equalsToken(symbol) && this.equalsName(symbol);
    }

    public boolean equalsName(Symbol symbol) {
        return this.getName().equals(symbol.getName());
    }

    public boolean equalsName(String name) {
        return this.getName().equals(name);
    }

    public boolean equalsType(SymbolType symbolType) {
        return this.getType().equals(symbolType);
    }

    public boolean equalsToken(Symbol symbol) {
        return this.equalsToken(symbol.getToken());
    }

    public boolean equalsToken(String token) {
        return this.getToken().equals(token);
    }

    public boolean equalsToken(char token) {
        return this.equalsToken(String.valueOf(token));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getToken());
    }

    @Override
    public String toString() {
        return "<" + this.getName() + ", \"" + this.getToken() + "\">";
    }

    public String name() {
        return name;
    }

    public String token() {
        return token;
    }

    public SymbolType type() {
        return type;
    }

}

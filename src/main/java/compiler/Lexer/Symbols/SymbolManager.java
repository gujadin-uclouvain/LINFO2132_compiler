package compiler.Lexer.Symbols;

import compiler.Lexer.Exception.LexerNotValidTokenException;
import compiler.Lexer.StringValue;

import java.util.HashMap;

public class SymbolManager {
    private final HashMap<String, Symbol> container;
    private final boolean populatedState;

    public SymbolManager() {
        this.container = new HashMap<>();
        this.populate();
        this.populatedState = true;
    }

    public void add(Symbol newSymbol) { this.container.putIfAbsent(newSymbol.getToken(), newSymbol); }

    public Symbol createIdentifier(String identifier) { return new Symbol("IDENTIFIER", identifier, SymbolType.IDENTIFIER); }
    public Symbol createValue(String value) {
        String type;
        if (StringValue.isInteger(value)) type = "INTEGER";
        else if (StringValue.isFloat(value)) type = "REAL";
        else if (StringValue.isBoolean(value)) type = "BOOLEAN";
        else if (StringValue.isString(value)) type = "STRING";
        else throw new LexerNotValidTokenException(value);
        return new Symbol(type+"_VALUE", value, SymbolType.VALUE);
    }

    public Symbol get(String input) {
        return this.container.get(input);
    }
    public boolean exists(String input) { return this.container.get(input) != null; }

    // Created by Parser
    public static Symbol createArrayType() { return new Symbol("ARRAY", "[]", SymbolType.ARRAY); }
    public static Symbol createBodyType() { return new Symbol("BODY", "", SymbolType.SPECIAL); }
    public static Symbol createBodyShadowType() { return new Symbol("BODY_SHADOW", "", SymbolType.SPECIAL); }
    public static Symbol createInitiatorType() { return new Symbol("INITIATOR", "", SymbolType.SPECIAL); }
    public static Symbol createProcInitiatorType() { return new Symbol("PROC_INITIATOR", "", SymbolType.SPECIAL); }
    public static Symbol createArgumentType() { return new Symbol("ARGUMENT", "", SymbolType.SPECIAL); }
    public static Symbol createFieldType() { return new Symbol("FIELD", "", SymbolType.SPECIAL); }
    public static Symbol createNullSymbol() { return new Symbol("NULL", null, SymbolType.SPECIAL); }
    public static Symbol createValueWithSign(Symbol sign, Symbol value) {
        return new Symbol(value.getName(), sign.getToken()+value.getToken(), value.getType());
    }

    public void populate() {
        if (this.populatedState) { return; }
        // Single-character tokens
        this.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        this.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        this.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        this.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        this.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        this.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));
        this.add(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER));
        this.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        this.add(new Symbol("COLON", ",", SymbolType.SINGLE_CHARACTER));

        // Special-character for values
        this.add(new Symbol("COMMA", "\"", SymbolType.SPECIAL_VALUE));

        // Operators
        this.add(new Symbol("PLUS", "+", SymbolType.OPERATOR));
        this.add(new Symbol("MINUS", "-", SymbolType.OPERATOR));
        this.add(new Symbol("TIMES", "*", SymbolType.OPERATOR));
        this.add(new Symbol("DIVIDE", "/", SymbolType.OPERATOR));
        this.add(new Symbol("MODULO", "%", SymbolType.OPERATOR));

        // Comparators
        this.add(new Symbol("GREATER", ">", SymbolType.COMPARATOR));
        this.add(new Symbol("EQUAL", "==", SymbolType.COMPARATOR));
        this.add(new Symbol("GREATER_EQUAL", ">=", SymbolType.COMPARATOR));
        this.add(new Symbol("LESS", "<", SymbolType.COMPARATOR));
        this.add(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR));
        this.add(new Symbol("DIFFERENT", "<>", SymbolType.COMPARATOR));

        // Types
        this.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        this.add(new Symbol("STRING", "string", SymbolType.TYPE));
        this.add(new Symbol("CHARACTER", "char", SymbolType.TYPE));
        this.add(new Symbol("REAL", "real", SymbolType.TYPE));
        this.add(new Symbol("BOOLEAN", "bool", SymbolType.TYPE));
        this.add(new Symbol("VOID", "void", SymbolType.TYPE));

        // Declarators
        this.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        this.add(new Symbol("CONST", "const", SymbolType.DECLARATOR));
        this.add(new Symbol("IMMUTABLE", "val", SymbolType.DECLARATOR));

        // Keywords
        this.add(new Symbol("RECORD", "record", SymbolType.KEYWORD));
        this.add(new Symbol("PROC", "proc", SymbolType.KEYWORD));
        this.add(new Symbol("FOR", "for", SymbolType.KEYWORD));
        this.add(new Symbol("WHILE", "while", SymbolType.KEYWORD));
        this.add(new Symbol("IF", "if", SymbolType.KEYWORD));
        this.add(new Symbol("ELSE", "else", SymbolType.KEYWORD));
        this.add(new Symbol("RETURN", "return", SymbolType.KEYWORD));
        this.add(new Symbol("TO", "to", SymbolType.KEYWORD));
        this.add(new Symbol("BY", "by", SymbolType.KEYWORD));

        // Logical
        this.add(new Symbol("AND", "and", SymbolType.LOGICAL));
        this.add(new Symbol("OR", "or", SymbolType.LOGICAL));

        // Assignment
        this.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));

        // Others
        this.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
        this.add(new Symbol("TAB", "\t", SymbolType.SPECIAL));
        this.add(new Symbol("NEWLINE", "\n", SymbolType.SPECIAL));
    }
}

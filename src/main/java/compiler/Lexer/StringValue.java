package compiler.Lexer;

public class StringValue {
    public static boolean isBoolean(String input) { return input.equals("false") || input.equals("true"); }
    public static boolean isString(String input) { return input.charAt(0) == '\"' && input.charAt(input.length()-1) == '\"'; }
    public static boolean isChar(String input) { return input.charAt(0) == '\'' && input.charAt(input.length()-1) == '\''; }
    public static boolean isInteger(String input) {
        if (!isNumerical(input)) return false;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == '.') return false;
        }
        return true;
    }
    public static boolean isFloat(String input) {
        if (!isNumerical(input)) return false;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == '.') return true;
        }
        return false;
    }
    public static boolean isNumerical(String input) {
        boolean isValue = true;
        if (input.isEmpty()) return false;
        if (input.charAt(0) == '.' || input.charAt(input.length()-1) == '.') return false;
        int nbrDots = 0;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == '.') nbrDots += 1;
            else isValue = isValue && Character.isDigit(c);
        }
        return isValue && nbrDots <= 1;
    }
}

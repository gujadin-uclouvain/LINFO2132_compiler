package compiler.Parser;

import compiler.Lexer.Symbols.Symbol;

import java.util.Arrays;

abstract class Analyzer extends Handler {
    private final String[] endExpressions;
    private final String[] endParameters;
    private final String[] endBlocks;
    private final String[] endArgs;
    private final Symbol[] symbols;
    private int index = 0;

    Analyzer(Symbol[] symbols) {
        this.symbols = symbols;
        this.endExpressions = new String[]{"SEMICOLON"};
        this.endParameters = new String[]{"COLON"};
        this.endBlocks = new String[]{"CLOSE_BRACE"};
        this.endArgs = new String[]{"CLOSE_PARENTHESIS"};
    }

    Symbol current() { return this.symbols[index]; }
    Symbol peekNext() { return !(index+1 >= this.symbols.length) ? this.symbols[index+1] : previous(); }
    Symbol previous() { return this.symbols[index-1]; }

    void increment() { if (!this.isEndOfExpression()) index++; }

    void nextExpression() {
        while (!this.isEndOfFile()) {
            if (isEndOfExpression() || isEndOfBlock()) { this.index++; break; }
            this.index++;
        }
    }
    void nextParameter() {
        while (!this.isEndOfFile()) {
            if (isEndOfParameter()) { this.index++; break; }
            if (isEndOfArgs()) { break; }
            this.index++;
        }
    }

    private boolean isEndOf(String[] comparators, Symbol symbol) {
        return Arrays.stream(comparators).anyMatch(symbol::equalsName);
    }
    boolean isEndOfParameter(Symbol symbol) { return this.isEndOf(this.endParameters, symbol); }
    boolean isEndOfParameter() { return isEndOfParameter(this.current()); }
    boolean isEndOfExpression(Symbol symbol) { return this.isEndOf(this.endExpressions, symbol); }
    boolean isEndOfExpression() { return isEndOfExpression(this.current()); }

    boolean isEndOfArgs(Symbol symbol) { return this.isEndOf(this.endArgs, symbol); }
    boolean isEndOfArgs() { return isEndOfArgs(this.current()); }
    boolean isEndOfBlock(Symbol symbol) { return this.isEndOf(this.endBlocks, symbol); }
    boolean isEndOfBlock() { return isEndOfBlock(this.current()); }
    boolean isEndOfFile() { return index >= this.symbols.length; }

    /**
     * Check if the current symbol is the same as symbolName (WITHOUT incrementing)
     */
    boolean isSimilar(Symbol symbol, String symbolName) {
        return !this.isEndOfExpression(symbol) && symbol.equalsName(symbolName);
    }
    boolean isSimilar(Symbol symbol, String[] symbolNames) {
        for (String symbolName : symbolNames) {
            if (this.isSimilar(symbol, symbolName)) {
                return true;
            }
        }
        return false;
    }
    boolean isSimilar(String symbolName) { return this.isSimilar(this.current(), symbolName); }
    boolean isSimilar(String[] symbolNames) { return this.isSimilar(this.current(), symbolNames); }

    /**
     * Check if the current symbol is the same as symbolName (WITH incrementing)
     */
    boolean isCorresponding(String symbolName) { return this.isCorresponding(new String[]{symbolName}); }
    boolean isCorresponding(String[] symbolNames) {
        for (String symbolName : symbolNames) {
            if (this.isSimilar(symbolName)) {
                this.increment();
                return true;
            }
        }
        return false;
    }

    boolean isNextCorresponding(String[] symbolNames) {
        for (String symbolName : symbolNames) {
            Symbol s = this.peekNext();
            if (this.isSimilar(s, symbolName)) {
                this.increment(); this.increment();
                return true;
            }
        }
        return false;
    }

    void handleCorrespondingError(String symbolName) {
        if (!this.isCorresponding(symbolName)) throw report(symbolName);
    }

    void handleSimilarError(String symbolName) {
        if (!this.isSimilar(symbolName)) throw report(symbolName);
    }

}

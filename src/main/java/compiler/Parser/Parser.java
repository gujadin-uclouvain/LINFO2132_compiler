package compiler.Parser;

import compiler.Lexer.Symbols.Symbol;
import compiler.Parser.Exception.ParserException;
import compiler.Parser.Structure.Expression;
import compiler.Parser.Structure.Block;

public class Parser {
    private final Grammar grammar;

    public Parser(Symbol[] symbols) {
        this.grammar = new Grammar(symbols);
    }

    public Expression getAST() {
        Expression p = new Block();
        while (!grammar.isEndOfFile()) {
//            try {
                p.addChild(this.grammar.expression());
//            } catch (ParserException e) {
//                p.addChild(null);
//            }
            this.grammar.nextExpression();
        }
        return p;
    }
}

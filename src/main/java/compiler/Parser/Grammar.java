package compiler.Parser;

import compiler.Lexer.Symbols.Symbol;
import compiler.Lexer.Symbols.SymbolManager;
import compiler.Lexer.Symbols.SymbolType;
import compiler.Parser.Exception.ParserSyntaxException;
import compiler.Parser.Structure.*;

public class Grammar extends Analyzer {

    Grammar(Symbol[] symbols) {
        super(symbols);
    }

    Expression expression() {
        return assign();
    }

    private Expression assign() {
        Expression exp = this.declarator();

        if (this.isCorresponding(Negotiator.ASSIGN.get())) {
            Symbol operator = this.previous();
            Expression right = this.logicalOR();
            exp = new Binary(operator, exp, right);
        }
        return exp;
    }

    private Expression declarator() {
        if (this.isCorresponding(Negotiator.DECLARATOR.get())) {
            Symbol operator = this.previous();
            Expression identifier = this.uniqueIdentifier();
            Expression exp = new Unary(operator, identifier);
            this.checkValidDeclarator(exp, this.current());
            return exp;
        }
        return this.logicalOR();
    }

    private Expression logicalOR() {
        Expression exp = this.logicalAND();

        while (this.isCorresponding(Negotiator.LOGICAL.get()[0])) {
            Symbol operator = this.previous();
            Expression right = this.logicalAND();
            exp = new Binary(operator, exp, right);
        }
        return exp;
    }

    private Expression logicalAND() {
        Expression exp = this.comparator();

        while (this.isCorresponding(Negotiator.LOGICAL.get()[1])) {
            Symbol operator = this.previous();
            Expression right = this.comparator();
            exp = new Binary(operator, exp, right);
        }
        return exp;
    }

    private Expression comparator() {
        Expression exp = this.weakOperator();

        while (this.isCorresponding(Negotiator.COMPARATOR.get())) {
            Symbol operator = this.previous();
            Expression right = this.weakOperator();
            exp = new Binary(operator, exp, right);
        }
        return exp;
    }

    private Expression weakOperator() {
        Expression exp = this.strongOperator();

        while (this.isCorresponding(Negotiator.WEAK_OPERATOR.get())) {
            Symbol operator = this.previous();
            Expression right = this.strongOperator();
            exp = new Binary(operator, exp, right);
        }
        return exp;
    }

    private Expression strongOperator() {
        Expression exp = this.recordFieldAccessOperator();

        while (this.isCorresponding(Negotiator.STRONG_OPERATOR.get())) {
            Symbol operator = this.previous();
            Expression right = this.recordFieldAccessOperator();
            exp = new Binary(operator, exp, right);
        }
        return exp;
    }

    private Expression recordFieldAccessOperator() {
        Expression exp = this.indexOperator();

        while (this.isCorresponding(Negotiator.RECORD_FIELD_ACCESS.get())) {
            Symbol operator = this.previous();
            Expression right = this.indexOperator();
            exp = new Binary(operator, exp, right);
        }
        return exp;
    }

    private Expression indexOperator() {
        Expression exp = this.parenthesis();

        if (this.isCorresponding(Negotiator.OPEN_ARRAY.get())) { // Is Array Type
            if (this.isCorresponding(Negotiator.CLOSE_ARRAY.get())) {
                Expression array = new Unary(SymbolManager.createArrayType(), exp);
                if (this.isCorresponding(Negotiator.OPEN_SEPARATOR.get())) {
                    Expression callerArgs = this.getArgsInCall();
                    this.handleCorrespondingError(Negotiator.CLOSE_SEPARATOR.get()[0]);
                    return new Binary(SymbolManager.createInitiatorType(), array, callerArgs);
                } return array;
            }
            Expression index = this.weakOperator();
            this.handleCorrespondingError(Negotiator.CLOSE_ARRAY.get()[0]);
            return new Binary(SymbolManager.createArrayType(), exp, index);
        }
        return exp;
    }

    private Expression parenthesis() {
        if (this.isCorresponding(Negotiator.OPEN_SEPARATOR.get())) {
            Expression exp = this.expression();
            this.handleCorrespondingError(Negotiator.CLOSE_SEPARATOR.get()[0]);
            return exp;
        }
        return this.unique();
    }

    private Expression unique() {
        if (this.isSimilar(Negotiator.VALUE.get())) return this.uniqueValue();
        if (this.isSimilar(Negotiator.WEAK_OPERATOR.get())) return this.uniqueOppositeValue();
        if (this.isSimilar(Negotiator.IDENTIFIER.get())) return this.uniqueIdentifier();
        if (this.isSimilar(Negotiator.TYPE.get())) return this.uniqueType();
        if (this.isSimilar(Negotiator.RETURN.get())) return this.uniqueReturn();
        if (this.isSimilar(Negotiator.IF.get())) return this.uniqueIf();
        if (this.isSimilar(Negotiator.WHILE.get())) return this.uniqueWhile();
        if (this.isSimilar(Negotiator.FOR.get())) return this.uniqueFor();
        if (this.isSimilar(Negotiator.PROCEDURE.get())) return this.uniqueProcedure();
        if (this.isSimilar(Negotiator.RECORD.get())) return this.uniqueRecord();
        throw this.report(null);
    }

    // Methods for special Keywords
    private Expression uniqueIdentifier() { return uniqueIdentifier(false); }
    private Expression uniqueIdentifier(boolean isProc) {
        if (this.isCorresponding(Negotiator.IDENTIFIER.get())) {
            Symbol identifier = this.previous();
            if (this.isCorresponding(Negotiator.OPEN_SEPARATOR.get())) {
                Expression callerName = new Literal(identifier);
                Expression callerArgs = this.getArgsInCall();
                this.handleCorrespondingError(Negotiator.CLOSE_SEPARATOR.get()[0]);
                return new Binary(isProc ? SymbolManager.createProcInitiatorType() : SymbolManager.createInitiatorType(), callerName, callerArgs);
            } else if (this.isSimilar(Negotiator.TYPE.get()) || this.isSimilar(Negotiator.IDENTIFIER.get())) {
                Expression type = this.indexOperator();
                return new Unary(identifier, type);
            } return new Literal(identifier);
        } throw this.report(Negotiator.IDENTIFIER.name());
    }

    private Expression getArgsInCall() {
        Expression args = new Block(SymbolManager.createArgumentType());
        while (!this.isEndOfArgs()) {
            args.addChild(this.expression());
            this.nextParameter();
        }
        return args;
    }

    private Expression uniqueValue() {
        if (this.isCorresponding(Negotiator.VALUE.get())) {
            return new Literal(this.previous());
        } throw this.report(Negotiator.VALUE.name());
    }

    private Expression uniqueOppositeValue() {
        if (this.isCorresponding(Negotiator.WEAK_OPERATOR.get())) {
            Symbol sign = this.previous();
            if (this.isCorresponding(Negotiator.VALUE.get())) {
                return new Literal(SymbolManager.createValueWithSign(sign, this.previous()));
            }
        } throw this.report(Negotiator.VALUE.name());
    }

    private Expression uniqueType() {
        if (this.isCorresponding(Negotiator.TYPE.get()) || this.isCorresponding(Negotiator.IDENTIFIER.get())) {
            return new Literal(this.previous());
        } throw this.report("");
    }

    private Expression uniqueReturn() {
        if (this.isCorresponding(Negotiator.RETURN.get())) {
            Symbol operator = this.previous();
            try {
                Expression exp = this.expression();
                return new Unary(operator, exp);
            } catch (ParserSyntaxException e) {
                return new Unary(operator, new Literal(SymbolManager.createNullSymbol()));
            }
        } throw this.report(Negotiator.RETURN.name());
    }

    // Procedure Methods
    private Expression uniqueProcedure() {
        if (this.isCorresponding(Negotiator.PROCEDURE.get())) {
            Symbol keyword = this.previous();
            Expression procedureName = this.uniqueIdentifier(true);
            Expression returnType = this.indexOperator();
            if (this.isCorresponding(Negotiator.OPEN_BLOCK.get())) {
                Expression body = this.getExpressionsInBlock(SymbolManager.createBodyShadowType());
                this.handleSimilarError(Negotiator.CLOSE_BLOCK.get()[0]);
                return new Ternary(keyword, procedureName, returnType, body);
            }
        } throw this.report(Negotiator.PROCEDURE.name());
    }

    // Record Methods
    private Expression uniqueRecord() {
        if (this.isCorresponding(Negotiator.RECORD.get())) {
            Symbol keyword = this.previous();
            Expression recordName = this.uniqueIdentifier();
            if (this.isCorresponding(Negotiator.OPEN_BLOCK.get())) {
                Expression body = this.getDeclaratorInRecordBlock();
                this.handleSimilarError(Negotiator.CLOSE_BLOCK.get()[0]);
                return new Binary(keyword, recordName, body);
            }
        } throw this.report(Negotiator.RECORD.name());
    }

    private Expression getDeclaratorInRecordBlock() {
        Expression body = new Block(SymbolManager.createFieldType());
        while (!this.isEndOfBlock()) {
            body.addChild(this.uniqueParameterDeclarator());
            this.nextExpression();
        }
        return body;
    }

    private Expression uniqueParameterDeclarator() {
        if (this.isCorresponding(Negotiator.IDENTIFIER.get())) {
            Symbol identifier = this.previous();
            Expression type = this.indexOperator();
            return new Unary(identifier, type);
        } throw this.report(Negotiator.IDENTIFIER.name());
    }


    // While methods
    private Expression uniqueWhile() {
        if (this.isCorresponding(Negotiator.WHILE.get())) {
            Symbol keyword = this.previous();
            Expression boolExpression = this.comparator();
            if (this.isCorresponding(Negotiator.OPEN_BLOCK.get())) {
                Expression body = this.getExpressionsInBlock(SymbolManager.createBodyType());
                this.handleSimilarError(Negotiator.CLOSE_BLOCK.get()[0]);
                return new Binary(keyword, boolExpression, body);
            }
        } throw this.report(Negotiator.WHILE.name());
    }

    // For methods
    private Expression uniqueFor() {
        if (this.isCorresponding(Negotiator.FOR.get())) {
            Symbol keyword = this.previous();
            Expression forSequence = this.uniqueForSequence();
            if (this.isCorresponding(Negotiator.OPEN_BLOCK.get())) {
                Expression body = this.getExpressionsInBlock(SymbolManager.createBodyType());
                this.handleSimilarError(Negotiator.CLOSE_BLOCK.get()[0]);
                return new Binary(keyword, forSequence, body);
            }
        } throw this.report(Negotiator.FOR.name());
    }

    private Expression uniqueForSequence() {
        Symbol sequence = new Symbol("FOR_SEQUENCE", "", SymbolType.SPECIAL);
        Expression assign = this.assign();
        if (this.isCorresponding(Negotiator.FOR_KEYWORD.get()[0])) {
            Expression limit = new Unary(this.previous(), this.weakOperator());
            if (this.isCorresponding(Negotiator.FOR_KEYWORD.get()[1])) {
                Expression inc = new Unary(this.previous(), this.weakOperator());
                return new Ternary(sequence, assign, limit, inc);
            }
        } throw this.report(Negotiator.FOR_KEYWORD.name());
    }

    // If methods
    private Expression uniqueIf() {
        if (this.isCorresponding(Negotiator.IF.get())) {
            Symbol keyword = this.previous();
            Expression boolExpression = this.comparator();
            if (this.isCorresponding(Negotiator.OPEN_BLOCK.get())) {
                Expression ifBody = this.getExpressionsInBlock(SymbolManager.createBodyType());

                this.handleSimilarError(Negotiator.CLOSE_BLOCK.get()[0]);

                if (this.isNextCorresponding(Negotiator.ELSE.get()) && this.isCorresponding(Negotiator.OPEN_BLOCK.get())) {
                    Expression elseBody = this.getExpressionsInBlock(SymbolManager.createBodyType());
                    this.handleSimilarError(Negotiator.CLOSE_BLOCK.get()[0]);
                    return new Ternary(keyword, boolExpression, ifBody, elseBody);
                }

                return new Binary(keyword, boolExpression, ifBody);
            }
        } throw this.report(Negotiator.IF.name());
    }

    // Blocks methods
    private Expression getExpressionsInBlock(Symbol blockSymbol) {
        Expression body = new Block(blockSymbol);
        while (!this.isEndOfBlock()) {
            body.addChild(this.expression());
            this.nextExpression();
        }
        return body;
    }
}

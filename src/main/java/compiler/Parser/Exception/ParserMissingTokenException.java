package compiler.Parser.Exception;

public class ParserMissingTokenException extends ParserSyntaxException {
    public ParserMissingTokenException(String message) {
        super(message);
    }
}

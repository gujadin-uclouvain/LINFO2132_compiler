package compiler.Parser.Exception;

public abstract class ParserException extends RuntimeException {
    public ParserException() {}
    public ParserException(String message) {
        super(message);
    }
}

package compiler.Parser.Exception;

import compiler.Lexer.Symbols.Symbol;

public class ParserSyntaxException extends ParserException {
    public ParserSyntaxException(String message) {
        super(message);
    }
}

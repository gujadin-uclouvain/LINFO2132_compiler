package compiler.Parser;

import compiler.Lexer.Symbols.Symbol;
import compiler.Lexer.Symbols.SymbolType;
import compiler.Parser.Exception.ParserException;
import compiler.Parser.Exception.ParserMissingTokenException;
import compiler.Parser.Exception.ParserSyntaxException;
import compiler.Parser.Structure.Expression;

import java.util.stream.Stream;

abstract class Handler {
    ParserException report(String symbolName) {
        String message = "";
        if (symbolName == null) {
            message = "The current symbol is misplaced";
        } else {//if (Negotiator.CLOSE_SEPARATOR.get()[0].equals(symbolName)) {
            message = "There is a missing "+symbolName;
        }
        return new ParserSyntaxException(message);
    }

    ParserException missingToken(String token) { return new ParserMissingTokenException("Missing token '"+token+"'"); }
    ParserException missing(String message) { return new ParserMissingTokenException(message); }


    // Declarator Verifications
    void checkValidDeclarator(Expression declaratorExpr, Symbol nextSymbol) {
        if (!this.isValidDeclarator(declaratorExpr)) { throw this.missing("Invalid Declarator"); }
        if (!nextSymbol.equalsType(SymbolType.ASSIGNMENT) && !nextSymbol.equalsName("SEMICOLON")) {
            throw this.missingToken("=");
        }
    }
    boolean isValidDeclarator(Expression expr) {
        return expr.getSymbol().equalsType(SymbolType.DECLARATOR) && !expr.isEmpty() &&
                isValidIdentifierWithType(expr.getChild(0));
    }
    boolean isValidIdentifierWithType(Expression expr) {
        return expr.getSymbol().equalsType(SymbolType.IDENTIFIER) && !expr.isEmpty() &&
               Stream.of(SymbolType.TYPE, SymbolType.ARRAY, SymbolType.IDENTIFIER)
                       .anyMatch(symbolType -> expr.getChild(0).getSymbol().equalsType(symbolType));
    }
}

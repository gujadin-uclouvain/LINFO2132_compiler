package compiler.Parser;

public enum Negotiator {
    // ARITY 0
        // UNIQUE
            NULL("NULL"),
            VALUE("INTEGER_VALUE", "REAL_VALUE", "BOOLEAN_VALUE", "STRING_VALUE"), // 0
            TYPE("INTEGER", "STRING", "CHARACTER", "REAL", "BOOLEAN", "VOID", "RECORD_TYPE"), // 0
            IDENTIFIER("IDENTIFIER"), // 0 (if identifier) OR 1 (if with type)

    // ARITY 1
        // UNIQUE
            RETURN("RETURN"), // 1
        // FOR ARGS
            FOR_KEYWORD("TO", "BY"), // 1
        // NEW SYMBOLS
            ARRAY("ARRAY"), // 1 (if constructor or array type) OR 2 (if index access)

    // ARITY 2
        // PRECEDENCE
            ASSIGN("ASSIGN"), // 2
            DECLARATOR("MUTABLE", "CONST", "IMMUTABLE"), // 2
            LOGICAL("OR", "AND"), // 2
            COMPARATOR("EQUAL", "DIFFERENT", "GREATER", "GREATER_EQUAL", "LESS", "LESS_EQUAL"), // 2
            WEAK_OPERATOR("PLUS", "MINUS"), // 2
            STRONG_OPERATOR("TIMES", "DIVIDE", "MODULO"), // 2
            RECORD_FIELD_ACCESS("DOT"), // 2

        // WITH BLOCK
            IF("IF"), // 2 (without else) OR 3 (with else)
            WHILE("WHILE"), // 2
            FOR("FOR"), // 2
            RECORD("RECORD"), // 2

        // NEW SYMBOLS
            INITIATOR("INITIATOR"), // 2 (record & array constructor AND function calls)
            PROC_INITIATOR("PROC_INITIATOR"), // 2 (Function initialization)

    // ARITY 3
        // WITH BLOCK
            PROCEDURE("PROC"), // 3

        // NEW SYMBOLS
            FOR_SEQUENCE("FOR_SEQUENCE"), // 3

    // ARITY N
        // NEW SYMBOLS
            BODY("BODY"), // N
            BODY_SHADOW("BODY_SHADOW"), // N (For PROC)
            ARGUMENT("ARGUMENT"), // N
            FIELD("FIELD"), // N

    // TO CREATE NEW SYMBOLS or UNUSED
        ELSE("ELSE"), // NULL
        OPEN_SEPARATOR("OPEN_PARENTHESIS"), CLOSE_SEPARATOR("CLOSE_PARENTHESIS"), // NULL
        OPEN_BLOCK("OPEN_BRACE"), CLOSE_BLOCK("CLOSE_BRACE"), // NULL
        OPEN_ARRAY("OPEN_BRACKET"), CLOSE_ARRAY("CLOSE_BRACKET"), // NULL
    ;

    private final String[] symbolNames;

    Negotiator(String... symbolNames) {
        this.symbolNames = symbolNames;
    }

    public String[] get() { return this.symbolNames; }
}

package compiler.Parser.Structure;

import compiler.Lexer.Symbols.Symbol;
import compiler.Lexer.Symbols.SymbolType;

public class Block extends Expression {
    public Block() {
        super(new Symbol("BLOCK", "", SymbolType.SPECIAL), Integer.MAX_VALUE);
    }
    public Block(Symbol symbol) { super(symbol, Integer.MAX_VALUE); }
    public Block(Expression... expressions) { this(); this.addChildren(expressions); }
    public Block(Symbol symbol, Expression... expressions) {
        this(symbol); this.addChildren(expressions);
    }
}

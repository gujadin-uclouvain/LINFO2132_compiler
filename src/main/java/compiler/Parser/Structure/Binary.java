package compiler.Parser.Structure;

import compiler.Lexer.Symbols.Symbol;

public class Binary extends Expression {
    public Binary(Symbol symbol) {
        super(symbol, 2);
    }
    public Binary(Symbol symbol, Expression left, Expression right) {
        super(symbol, 2);
        this.addChild(left);
        this.addChild(right);
    }
}

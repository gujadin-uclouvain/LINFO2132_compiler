package compiler.Parser.Structure;

import compiler.Lexer.Symbols.Symbol;

public class Ternary extends Expression {
    public Ternary(Symbol symbol) {
        super(symbol, 3);
    }
    public Ternary(Symbol symbol, Expression left, Expression middle, Expression right) {
        super(symbol, 3);
        this.addChild(left);
        this.addChild(middle);
        this.addChild(right);
    }
}

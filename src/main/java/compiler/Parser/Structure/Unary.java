package compiler.Parser.Structure;

import compiler.Lexer.Symbols.Symbol;

public class Unary extends Expression {
    public Unary(Symbol symbol) {
        super(symbol, 1);
    }
    public Unary(Symbol symbol, Expression right) {
        super(symbol, 1);
        this.addChild(right);
    }
}

package compiler.Parser.Structure;

import compiler.CodeGeneration.ExpressionBytecodeVisitor;
import compiler.CodeGeneration.Tools.MethodData;
import compiler.Lexer.Symbols.Symbol;
import compiler.Semantic.ExpressionSemanticVisitor;
import compiler.Tools.Context.Context;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashMap;

public abstract class Expression {
    private final Symbol symbol;
    private final ArrayList<Expression> children;
    private final int arity;
    private int currentIndex = -1;

    public Expression(){
        this.symbol = null;
        this.arity = Integer.MAX_VALUE;
        this.children = new ArrayList<>();
    }
    public Expression(Symbol symbol, int arity){
        this.symbol = symbol;
        this.arity = arity;
        this.children = new ArrayList<>();
    }

    public boolean isRoot() { return this.symbol == null; }

    public boolean isLeaf(){
        return this.arity == 0;
    }

    public boolean isFull() { return this.children.size() >= this.arity; }
    public boolean isEmpty() { return this.children.size() == 0; }

    public boolean addChild(Expression child){
        if (!this.isFull()) {
            this.currentIndex++;
            return this.children.add(child);
        }
        return false;
    }

    public boolean addChildren(Expression... children){
        boolean flag = true;
        for (Expression child : children) {
            flag = flag && this.addChild(child);
        }
        return flag;
    }

    public Expression getChild(int index) {
        if (this.children.size() <= 0 || index > this.currentIndex+1) return null;
        return this.children.get(index);
    }

    public Expression popChild() {
        if (!this.isEmpty()) {
            return this.children.remove(this.currentIndex--);
        }
        throw new EmptyStackException();
    }

    public Expression[] getChildren(){
        return this.children.toArray(new Expression[0]);
    }

    public Symbol getSymbol(){
        return this.symbol;
    }

    public int getArity() { return this.arity; };

    public void invertPrecedence(Expression expression) {
        expression.addChild(this.popChild());
        this.addChild(expression);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Expression that)) return false;
        if (getArity() != that.getArity()) return false;
        if (!getSymbol().equals(that.getSymbol())) return false;
        if (!getSymbol().equalsName(that.getSymbol())) return false;

        for (int i = 0; i < this.children.size(); i++) {
            if (!this.getChild(i).equals(that.getChild(i))) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if (this.getSymbol() != null) return this.getSymbol().toString();
        return "";
    }

    public Object checkSemantic(ExpressionSemanticVisitor visitor, Context context) {
        if (this instanceof Literal l) return visitor.visitArity0(l, context);
        if (this instanceof Unary u) return visitor.visitArity1(u, context);
        if (this instanceof Binary b) return visitor.visitArity2(b, context);
        if (this instanceof Ternary t) return visitor.visitArity3(t, context);
        if (this instanceof Block b) return visitor.visitArityN(b, context);
        throw new RuntimeException("Expression Type unknown");
    }

    public Object accept(ExpressionBytecodeVisitor visitor, Context context, MethodData currMethod, HashMap<String, byte[]> bytecodes) {
        if (this instanceof Literal l) return visitor.visitArity0(l, context, currMethod, bytecodes);
        if (this instanceof Unary u) return visitor.visitArity1(u, context, currMethod, bytecodes);
        if (this instanceof Binary b) return visitor.visitArity2(b, context, currMethod, bytecodes);
        if (this instanceof Ternary t) return visitor.visitArity3(t, context, currMethod, bytecodes);
        if (this instanceof Block b) return visitor.visitArityN(b, context, currMethod, bytecodes);
        throw new RuntimeException("Expression Type unknown");
    }
}

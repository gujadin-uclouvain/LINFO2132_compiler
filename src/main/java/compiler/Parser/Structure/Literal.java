package compiler.Parser.Structure;

import compiler.Lexer.Symbols.Symbol;

public class Literal extends Expression {
    public Literal(Symbol symbol) {
        super(symbol, 0);
    }
}
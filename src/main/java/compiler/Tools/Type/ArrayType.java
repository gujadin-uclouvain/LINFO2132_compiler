package compiler.Tools.Type;

public class ArrayType extends BasicType {
    private int length = Integer.MAX_VALUE;

    public ArrayType(String type) { super(type); }
    public ArrayType(String type, int length) { super(type); this.setLength(length); }

    public void setLength(int length) {
        if (this.length == Integer.MAX_VALUE) {
            if (length <= 0) throw new RuntimeException("Declaration Error: The array length must be greater than 0");
            this.length = length;
        } else throw new RuntimeException("Assign Error: The array length is immutable");
    }
    public int length() { return this.length; }

    @Override
    public String toString() {
        if (this.length == 0) return ""+this.getTypeID()+"_ARRAY";
        return ""+this.getTypeID()+"_ARRAY("+length+")";
    }
}

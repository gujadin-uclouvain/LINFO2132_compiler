package compiler.Tools.Type;

import compiler.Tools.Context.Structure.ArgumentsContainer;
import compiler.Tools.Context.Structure.FieldsContainer;
import compiler.Tools.Context.Structure.Variable;

import java.util.*;

public class RecordType extends BasicType {
    protected LinkedHashSet<Variable> fields;

    public RecordType(String type) {
        super(type);
        this.fields = new LinkedHashSet<>();
    }

    public BasicType add(Variable variable) {
        this.fields.add(variable);
        return variable.getData().getType();
    }

    public void addAll(FieldsContainer newFields) {
        this.fields.addAll(Arrays.asList(newFields.getContent()));
    }
    public void addAll(ArgumentsContainer newArguments) {
        for (Object o : newArguments.getContent()) {
            if (o instanceof Variable variable) {
                this.fields.add(variable);
            } else throw new RuntimeException("Argument Error: The argument '"+o+"' must be a valid argument (with type and identifier)");
        }
    }

    public BasicType get(String identifier) {
        return this.fields.stream().filter(variable -> variable.getIdentifier().equals(identifier)).findFirst().map(variable -> variable.getData().getType()).orElse(null);
    }

    public Variable[] getAll() {
        return this.fields.toArray(Variable[]::new);
    }

    public int size() {
        return this.fields.size();
    }

    public boolean compare(Object[] arr) {
        if (this.size() != arr.length) throw new RuntimeException("Argument Error: Number of arguments mismatches: "+ this.fields + " had " + Arrays.toString(arr));
        int i = 0;
        for (Variable variable : this.fields) {
            if (arr[i] instanceof ProcedureType type) {
                if (!variable.getData().getType().equals(type.getReturnType())) {
                    throw new RuntimeException("Arguments Error: Expected for "+ getTypeID()+"."+variable+" : "+variable.getData().getType()+" not compatible with "+type);
                }

            } else if (arr[i] instanceof BasicType type) {
                if (!variable.getData().getType().equals(type)) {
                    throw new RuntimeException("Arguments Error: Expected for "+ getTypeID()+"."+variable+" : "+variable.getData().getType()+" not compatible with "+type);
                }
            }
            i++;
        }
        return true;
    }

    public Iterator<Variable> iterator() {
        return this.fields.iterator();
    }

    @Override
    public String toString() {
        return getTypeID() + fields;
    }
}

package compiler.Tools.Type;

public class BasicType {
    private final String type;

    public BasicType(String type) {
        this.type = type;
    }

    public String getTypeID() { return this.type; }

    public boolean is(String strType) { return this.type.equals(strType); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasicType basicType)) return false;
        return this.getTypeID().equals(basicType.getTypeID());
    }

    @Override
    public String toString() {
        return this.getTypeID();
    }
}

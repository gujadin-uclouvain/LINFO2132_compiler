package compiler.Tools.Type;

import compiler.Tools.Context.Context;
import compiler.Tools.Context.Structure.Variable;
import compiler.Tools.Context.Structure.VariableData;

import java.util.Arrays;
import java.util.Iterator;

public class ProcedureType extends RecordType {
    private BasicType returnType;

    public ProcedureType(String type) { super(type); }
    public ProcedureType(String type, BasicType returnType) {
        this(type);
        this.addReturnType(returnType);
    }

    public ProcedureType(String type, BasicType returnType, Variable ...args) {
        this(type, returnType);
        Arrays.stream(args).forEachOrdered(this::add);
    }

    public boolean checkReturnType(BasicType returnType) { return this.returnType.equals(returnType); }
    public BasicType getReturnType() { return this.returnType; }
    public void addReturnType(BasicType returnType) {
        if (this.returnType == null) {
            this.returnType = returnType;
        } else throw new RuntimeException("Assign Error: The return value cannot be change");
    }

    public void addArgsToContext(Context context) {
        Iterator<Variable> iter = this.iterator();
        while (iter.hasNext()) {
            Variable nextArg = iter.next();
            context.add(nextArg.getIdentifier(), new VariableData(nextArg.getData().getType(), nextArg.getData().getContent(), "MUTABLE"));
        }
    }

    public BasicType[] getArgsType() {
        BasicType[] res = new BasicType[this.size()];
        int idx = 0;
        Iterator<Variable> iter = this.iterator();
        while (iter.hasNext()) {
            Variable nextArg = iter.next();
            res[idx++] = nextArg.getData().getType();
        }
        return res;
    }

    @Override
    public String toString() {
        return super.toString() + " => " + this.returnType;
    }
}

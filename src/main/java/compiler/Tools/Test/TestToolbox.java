package compiler.Tools.Test;

import compiler.CodeGeneration.BytecodeCompiler;
import compiler.Lexer.Lexer;
import compiler.Lexer.Symbols.Symbol;
import compiler.Parser.Structure.Expression;
import compiler.Parser.Parser;
import compiler.Semantic.Semantic;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

public class TestToolbox {
    private TestToolbox(){}

    public static ArrayList<Symbol> getLexerOut(String input, boolean verbose) throws IOException {
        StringReader reader = new StringReader(input);
        Lexer lexer = new Lexer(reader);
        ArrayList<Symbol> symbols = new ArrayList<>();
        while (!lexer.isEnding()) {
            Symbol s = lexer.getNextSymbol();
            if (s != null) symbols.add(s);
            else if(verbose) System.out.println(symbols);
        }
        if(verbose) System.out.println(symbols);
        return symbols;
    }

    public static Expression getParserOut(String input, boolean verbose) throws IOException {
        Symbol[] symbols = TestToolbox.getLexerOut(input, verbose).toArray(Symbol[]::new);
        Parser parser = new Parser(symbols);
        return parser.getAST();
    }

    public static Expression analyseSemantic(String input, boolean verbose) throws IOException {
        Expression rootAST = getParserOut(input, verbose);
        Semantic checker = new Semantic();
        checker.analyse(rootAST);
        return rootAST;
    }

    public static void testCompiledCode(String input, boolean verbose) throws IOException {
        Expression rootAST = analyseSemantic(input, verbose);
        BytecodeCompiler compiler = new BytecodeCompiler();
        compiler.generate(rootAST);
        compiler.test();
    }
}

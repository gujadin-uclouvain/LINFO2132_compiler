package compiler.Tools;

public class Triplet<T1, T2, T3> extends Pair<T1, T2>{
    private final T3 elem3;

    public Triplet(T1 elem1, T2 elem2, T3 elem3) {
        super(elem1, elem2);
        this.elem3 = elem3;
    }

    public T3 get3() { return elem3; }
}

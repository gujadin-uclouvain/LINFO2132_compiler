package compiler.Tools;

public class Pair<T1, T2> {
    private final T1 elem1;
    private final T2 elem2;

    public Pair(T1 elem1, T2 elem2) {
        this.elem1 = elem1;
        this.elem2 = elem2;
    }

    public T1 get1() { return elem1; }
    public T2 get2() { return elem2; }
}

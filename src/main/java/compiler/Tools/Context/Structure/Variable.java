package compiler.Tools.Context.Structure;

import compiler.Tools.Type.BasicType;

import java.util.Objects;

public class Variable {
    private final String identifier;
    private final VariableData data;

    public Variable(String identifier, VariableData data) {
        this.identifier = identifier;
        this.data = data;
    }
    public Variable(String identifier, BasicType type) {
        this(identifier, new VariableData(type));
    }
    public Variable(String identifier, BasicType type, String declarator) {
        this(identifier, new VariableData(type, declarator));
    }
    public Variable(String identifier, BasicType type, Object content, String declarator) {
        this(identifier, new VariableData(type, content, declarator));
    }

    public String getIdentifier() { return identifier; }
    public VariableData getData() { return data; }

    public boolean isProcedure() { return data.getDeclarator().equals("PROCEDURE"); }
    public boolean isRecord() { return data.getDeclarator().equals("RECORD"); }

    @Override
    public String toString() {
        return "[" + identifier + ", " + data + ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Variable variable)) return false;
        return getIdentifier().equals(variable.getIdentifier());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdentifier());
    }
}

package compiler.Tools.Context.Structure;

import java.util.ArrayList;

public class ArgumentsContainer {
    private final ArrayList<Object> container;

    public ArgumentsContainer() {
        this.container = new ArrayList<>();
    }

    public boolean add(Object o) {
        return this.container.add(o);
    }

    public Object get(int index) {
        return container.get(index);
    }

    public Object[] getContent() {
        return container.toArray();
    }
}

package compiler.Tools.Context.Structure;

import compiler.Tools.Type.BasicType;

public class VariableData {
    private final BasicType type;
    private final Object content;
    private String declarator;

    public VariableData(BasicType type, Object content, String declarator) {
        this.type = type;
        this.content = content;
        this.declarator = declarator;
    }
    public VariableData(BasicType type, String declarator) { this(type, null, declarator); }
    public VariableData(BasicType type) { this(type, null, null); }

    public BasicType getType() { return type; }
    public Object getContent() { return content; }
    public String getDeclarator() { return declarator; }

    public void setDeclarator(String declarator) { if (this.declarator == null) { this.declarator = declarator; } }

    @Override
    public String toString() {
        return "[" + type + ", " + content + ", " + declarator + ']';
    }
}

package compiler.Tools.Context.Structure;

import java.util.LinkedHashSet;

public class FieldsContainer {
    private final LinkedHashSet<Variable> container;

    public FieldsContainer() {
        this.container = new LinkedHashSet<>();
    }

    public boolean add(Variable v) {
        return this.container.add(v);
    }

    public Variable[] getContent() {
        return container.toArray(Variable[]::new);
    }
}

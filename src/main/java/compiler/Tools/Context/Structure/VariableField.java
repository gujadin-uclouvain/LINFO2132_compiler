package compiler.Tools.Context.Structure;

public class VariableField extends Variable {
    private final String recordName;

    public VariableField(String fieldName, String recordName, VariableData data) {
        super(fieldName, data);
        this.recordName = recordName;
    }

    public String getRecord() {
        return recordName;
    }
}

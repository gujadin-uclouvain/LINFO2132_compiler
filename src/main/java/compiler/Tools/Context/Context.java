package compiler.Tools.Context;

import compiler.Tools.Context.Structure.Variable;
import compiler.Tools.Context.Structure.VariableData;
import compiler.Tools.Type.BasicType;
import compiler.Tools.Type.ProcedureType;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Context {
    private final boolean shadow;
    private final LinkedHashMap<String, VariableData> store;
    private String lastDeclared = "NONE";

    public Context() { this.store = new LinkedHashMap<>(); this.shadow = false;}
    public Context(boolean shadow) { this.store = new LinkedHashMap<>(); this.shadow = shadow;}
    public Context(Context context, boolean shadow) { this.store = new LinkedHashMap<>(context.getStore()); this.shadow = shadow; }

    public boolean isShadow() { return this.shadow; }
    private LinkedHashMap<String, VariableData> getStore() { return this.store; }
    public VariableData get(String key) {
        return this.store.get(key);
    }
    public VariableData getLast() {
        Iterator<Map.Entry<String, VariableData>> iter = this.store.entrySet().iterator();
        VariableData ret = null;
        while (iter.hasNext()) { ret = iter.next().getValue(); }
        return ret;
    }

    public VariableData add(String key, VariableData data) {
        return this.add(key, data, false);
    }
    public VariableData add(String key, VariableData data, boolean isNew) {
        if (!this.shadow && this.store.containsKey(key) && isNew) throw new RuntimeException("Already defined identifier "+key);
        return this.store.put(key, data);
    }

    public void addProcedure(String procName, BasicType returnType, Variable... args) {
        this.add(procName, new VariableData(
                new ProcedureType(procName, returnType, args),
                null, "PROCEDURE"
        ));
    }

    public void checkAndSetDeclarator(String key, String declarator) {
        VariableData data = this.get(key);
        switch (declarator) {
            case "CONST": if (!lastDeclared.equals("NONE") && !lastDeclared.equals("CONST"))
                throw new RuntimeException("CONST "+key+" must be declared at the beginning of the file");
            case "RECORD": if (!lastDeclared.equals("NONE") && !lastDeclared.equals("CONST") && !lastDeclared.equals("RECORD"))
                throw new RuntimeException("RECORD "+key+" must be declared after CONST or other RECORD");
        }
        lastDeclared = declarator;
        this.add(key, new VariableData(data.getType(), data.getContent(), declarator));
    }

    public void intersect(Context globalContext) {
        this.store.keySet().retainAll(globalContext.getStore().keySet()); // Get intersect between all keys
        for (String key : this.store.keySet()) { // Put all keys from intersect into globalContext
            globalContext.getStore().replace(key, this.get(key));
//            System.out.println(key + " : " + globalContext.get(key));
        }
    }
}

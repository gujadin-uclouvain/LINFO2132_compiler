package compiler.CodeGeneration;

import compiler.CodeGeneration.Tools.MethodData;
import compiler.Lexer.Symbols.Symbol;
import compiler.Parser.Negotiator;
import compiler.Parser.Structure.*;
import compiler.Tools.Context.Context;
import compiler.Tools.Context.Structure.*;
import compiler.Semantic.Exception.SemanticUnknownSymbolException;
import compiler.Tools.Pair;
import compiler.Tools.Triplet;
import compiler.Tools.Type.*;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;

import java.util.HashMap;

import static org.objectweb.asm.Opcodes.*;

class BytecodeGenerator extends ASMTools implements ExpressionBytecodeVisitor {
    private final ClassWriter creator;

    public BytecodeGenerator(ClassWriter env) { this.creator = env; }

    @Override
    public Object visitArity0(Literal expression, Context context, MethodData currMethod, HashMap<String, byte[]> bytecodes) {
        Symbol symbol = expression.getSymbol();
        if (isSimilar(symbol, Negotiator.VALUE)) {
            return castSymbolValue(symbol);

        } if (isSimilar(symbol, Negotiator.TYPE)) {
            return typeStringToObject(symbol.getName(), context);

        } if (isSimilar(symbol, Negotiator.IDENTIFIER)) {
            VariableData identifierData = context.get(symbol.getToken());
            if (identifierData == null) throw new RuntimeException("Not declared identifier "+symbol.getToken());
            return new Variable(symbol.getToken(), identifierData);

        } if (isSimilar(symbol, Negotiator.NULL)) {
            return symbol.getToken();
        }
        throw new SemanticUnknownSymbolException(symbol);
    }

    @Override
    public Object visitArity1(Unary expression, Context context, MethodData currMethod, HashMap<String, byte[]> bytecodes) {
        Symbol symbol = expression.getSymbol();
        Object right = expression.getChild(0).accept(this, context, currMethod, bytecodes);
        if (isSimilar(symbol, Negotiator.IDENTIFIER)) { // With Type (Variable Declaration)
            VariableData identifierData;
            if (right instanceof Variable v) {
                identifierData = initNewVariable(currMethod, typeObjectToType(context.get(v.getIdentifier()).getType()));
            } else identifierData = initNewVariable(currMethod, typeObjectToType(right));
            context.add(symbol.getToken(), identifierData, true);
            return new Variable(symbol.getToken(), identifierData);

        } if (isSimilar(symbol, Negotiator.DECLARATOR)) {
            if (right instanceof Variable vRight) {
                context.checkAndSetDeclarator(vRight.getIdentifier(), symbol.getName());
                vRight.getData().setDeclarator(symbol.getName());
                return right;
            } throw new RuntimeException("Declarator Error: Missing identifier with declarator "+symbol.getName());

        } if (isSimilar(symbol, Negotiator.RETURN)) {
            BasicType rightType;
            if (right instanceof Variable vRight) { // Variable / call in right
                loadVariable(currMethod.getMethod(), vRight);
                rightType = vRight.getData().getType();
            } else if (right == null) {
                currMethod.getMethod().visitCode();
                currMethod.getMethod().visitInsn(RETURN);
                currMethod.getMethod().visitEnd();
                return null;
            } else {
                loadValue(currMethod.getMethod(), right);
                rightType = typeObjectToType(right);
            }
            returnLastInStack(currMethod.getMethod(), rightType);
            return null;


        } if (isSimilar(symbol, Negotiator.FOR_KEYWORD)) {
            return right;

        } if (isSimilar(symbol, Negotiator.ARRAY)) { // Declaration OR Constructor
            return new ArrayType(typeObjectToString(right));

        } return null;
    }

    private void addRecordProcedureIdentifierInContext(Binary expression, Context context) {
        if (isSimilar(expression.getSymbol(), new Negotiator[]{Negotiator.RECORD, Negotiator.PROC_INITIATOR})) {
            Symbol record = expression.getChild(0).getSymbol();
            if (isSimilar(expression.getSymbol(), Negotiator.RECORD)) { // Is Record Type
                context.add(record.getToken(), new VariableData(new RecordType(record.getToken())));
                context.checkAndSetDeclarator(record.getToken(), "RECORD");
            } else {                                    // Is Procedure Type
                context.add(record.getToken(), new VariableData(new ProcedureType(record.getToken())));
                context.checkAndSetDeclarator(record.getToken(), "PROCEDURE");
            }
        }
    }

    @Override
    public Object visitArity2(Binary expression, Context context, MethodData currMethod, HashMap<String, byte[]> bytecodes) {
        Symbol symbol = expression.getSymbol();
        addRecordProcedureIdentifierInContext(expression, context);
        Object left;
        Object right;

        if (isSimilar(symbol, Negotiator.IF)) {
            left = expression.getChild(0).accept(this, context, currMethod, bytecodes);
            loadVisitedElemInStack(currMethod.getMethod(), left);

            // Jump at the end of the condition
            Label endLabel = new Label();
            currMethod.getMethod().visitJumpInsn(IFEQ, endLabel);

            // Visit If body
            right = expression.getChild(1).accept(this, context, currMethod, bytecodes);

            currMethod.getMethod().visitLabel(endLabel);


        } else if (isSimilar(symbol, Negotiator.WHILE)) {
            currMethod.getMethod().visitCode();
            // Start loop
            Label loopLabel = new Label();
            currMethod.getMethod().visitLabel(loopLabel);
            currMethod.getMethod().visitEnd();

            left = expression.getChild(0).accept(this, context, currMethod, bytecodes);
            loadVisitedElemInStack(currMethod.getMethod(), left);

            currMethod.getMethod().visitCode();
            // Jump at the end of the loop
            Label endLabel = new Label();
            currMethod.getMethod().visitJumpInsn(IFEQ, endLabel);

            right = expression.getChild(1).accept(this, context, currMethod, bytecodes);

            currMethod.getMethod().visitJumpInsn(GOTO, loopLabel);

            currMethod.getMethod().visitLabel(endLabel);
            currMethod.getMethod().visitEnd();


        } else if (isSimilar(symbol, Negotiator.FOR)) {
            left = expression.getChild(0).accept(this, context, currMethod, bytecodes);
            if (!(left instanceof Triplet triplet)) throw new RuntimeException("");

            currMethod.getMethod().visitCode();
            // Start loop
            Label loopLabel = new Label();
            currMethod.getMethod().visitLabel(loopLabel);

            loadVisitedElemInStack(currMethod.getMethod(), triplet.get1());
            loadVisitedElemInStack(currMethod.getMethod(), triplet.get2());

            applyOperationInt(currMethod.getMethod(), "<");

            // Jump at the end of the loop
            Label endLabel = new Label();
            currMethod.getMethod().visitJumpInsn(IFEQ, endLabel);

            right = expression.getChild(1).accept(this, context, currMethod, bytecodes);

            loadVisitedElemInStack(currMethod.getMethod(), triplet.get1());
            loadVisitedElemInStack(currMethod.getMethod(), triplet.get3());

            currMethod.getMethod().visitInsn(IADD);
            storeInVariable(currMethod.getMethod(), (Variable) triplet.get1());

            // Goto the start of the loop
            currMethod.getMethod().visitJumpInsn(GOTO, loopLabel);

            currMethod.getMethod().visitLabel(endLabel);
            currMethod.getMethod().visitEnd();

        } else {
            left = expression.getChild(0).accept(this, context, currMethod, bytecodes);
            if (isSimilar(symbol, Negotiator.RECORD_FIELD_ACCESS)) { // Access record field (must be already declared)
                if (!(left instanceof Variable vLeft)) throw new RuntimeException("Identifier for record field access is not valid: "+left);
                VariableData recInstance = vLeft.getData();
                if (!(recInstance.getType() instanceof RecordType recordType))  throw new RuntimeException("Identifier "+vLeft.getIdentifier()+" is not a record");
                String field = expression.getChild(1).getSymbol().getToken();
                BasicType fieldType = recordType.get(field);

                loadVisitedElemInStack(currMethod.getMethod(), vLeft);
                currMethod.getMethod().visitFieldInsn(GETFIELD,
                        recordType.getTypeID(),
                        field,
                        getType(fieldType)
                );

                // Add the result in a VariableField
                VariableField result = new VariableField(field, recordType.getTypeID(),
                        new VariableData(fieldType, 255-nbrTempVariable++, "|SYSTEM|")
                );
//                Variable result = new Variable("|TEMP|", fieldType, 255-nbrTempVariable++, "|SYSTEM|");
                storeInVariable(currMethod.getMethod(), result);

                return result;
            }
            right = expression.getChild(1).accept(this, context, currMethod, bytecodes);
        }

        if (isSimilar(symbol, new Negotiator[]{Negotiator.STRONG_OPERATOR, Negotiator.WEAK_OPERATOR, Negotiator.LOGICAL, Negotiator.COMPARATOR})) {
            currMethod.getMethod().visitCode();

            loadVisitedElemInStack(currMethod.getMethod(), left);
            loadVisitedElemInStack(currMethod.getMethod(), right);
            BasicType lType = typeObjectToType(left);

            BasicType returnType = integerType();
            if (lType.equals(integerType())) {
                returnType = applyOperationInt(currMethod.getMethod(), symbol.getToken());
            } if (lType.equals(realType())) {
                returnType = applyOperationReal(currMethod.getMethod(), symbol.getToken());
            } if (lType.equals(booleanType())) {
                returnType = applyOperationBool(currMethod.getMethod(), symbol.getToken());
            }

            // Add the result in a |TEMP| Variable
            Variable result = new Variable("|TEMP|", returnType, 255-nbrTempVariable++, "|SYSTEM|");
            storeInVariable(currMethod.getMethod(), result);

            currMethod.getMethod().visitEnd();
            return result;

        } if (isSimilar(symbol, new Negotiator[]{Negotiator.ASSIGN})) {
            if (!(left instanceof Variable vLeft)) throw new RuntimeException("There must be a VARIABLE before an ASSIGN");
            if (right instanceof Variable vRight) { // Variable / call in right
                if (!vRight.isProcedure() && !vRight.isRecord()) {
                    loadVariable(currMethod.getMethod(), vRight);
                }
            } else if (right != null) loadValue(currMethod.getMethod(), right);
            if (left instanceof VariableField vFieldLeft) {
                currMethod.getMethod().visitFieldInsn(PUTFIELD,
                        vFieldLeft.getRecord(),
                        vFieldLeft.getIdentifier(),
                        getType(vFieldLeft.getData().getType())
                );

            } else {
                storeInVariable(currMethod.getMethod(), vLeft);
                if (right instanceof Variable vRight && vRight.isRecord()) {
                    loadVariable(currMethod.getMethod(), vLeft);
                }
            }
            return left;

        } if (isSimilar(symbol, Negotiator.ARRAY)) { // Access index (must be already declared)

        } if (isSimilar(symbol, Negotiator.INITIATOR)) {
            if (!(left instanceof Variable vLeft)) { throw new RuntimeException("There must be a VARIABLE when a INITIATOR occurs"); }
            if (!(right instanceof ArgumentsContainer acRight)) { throw new RuntimeException("a PROCEDURE needs ARGUMENTS, NOT "+right); }
            if (vLeft.isProcedure()) {
                invokeMethod(currMethod.getMethod(), vLeft, acRight);
            } else if (vLeft.isRecord()) {
                invokeClass(currMethod.getMethod(), vLeft, acRight);
            } else throw new RuntimeException("The INITIATOR must be a PROCEDURE or a RECORD");
            return left;

        } if (isSimilar(symbol, new Negotiator[]{Negotiator.RECORD, Negotiator.PROC_INITIATOR})) {
            if (left instanceof Variable vLeft) {
                String name = vLeft.getIdentifier();
                VariableData record = context.get(name);
                if (record.getType() instanceof RecordType r) {
                    currMethod.resetLocalStackID();
                    if (right instanceof FieldsContainer container) {
                        r.addAll(container);
                        bytecodes.put(name, createClass(name, container.getContent()));
                    }
                    if (right instanceof ArgumentsContainer container) {
                        r.addAll(container);
                    }
                    return new Variable(name, record);
                }
            }

        } return null;
    }

    @Override
    public Object visitArity3(Ternary expression, Context context, MethodData currMethod, HashMap<String, byte[]> bytecodes) {
        Symbol symbol = expression.getSymbol();
        Object left = expression.getChild(0).accept(this, context, currMethod, bytecodes);
        Object middle;
        Object right;

        if (isSimilar(symbol, Negotiator.IF)) {
            loadVisitedElemInStack(currMethod.getMethod(), left);

            // Jump to else condition
            Label elseLabel = new Label();
            currMethod.getMethod().visitJumpInsn(IFEQ, elseLabel);

            // Visit If body
            middle = expression.getChild(1).accept(this, context, currMethod, bytecodes);

            // Jump at the end of the condition
            Label endLabel = new Label();
            currMethod.getMethod().visitJumpInsn(GOTO, endLabel);

            currMethod.getMethod().visitLabel(elseLabel);
            // Visit Else body
            right = expression.getChild(2).accept(this, context, currMethod, bytecodes);
            currMethod.getMethod().visitLabel(endLabel);

        } else {
            middle = expression.getChild(1).accept(this, context, currMethod, bytecodes);
            MethodData newMethod = currMethod;
            if (isSimilar(symbol, new Negotiator[]{Negotiator.PROCEDURE})) {
                if (left instanceof Variable proc && proc.getData().getType() instanceof ProcedureType type) {
                    type.addReturnType(typeObjectToType(middle));
                    newMethod = new MethodData(
                            createMethod(creator, proc.getIdentifier(), createDescriptor(type.getReturnType(), type.getArgsType()),
                                    ACC_PUBLIC | ACC_STATIC),
                            type.size()
                    );
                }
            }
            right = expression.getChild(2).accept(this, context, newMethod, bytecodes);
            if (isSimilar(symbol, new Negotiator[]{Negotiator.PROCEDURE})) {
                newMethod.getMethod().visitMaxs(-1, -1);
            }
        }

        // ------------------------------------------------------------------ //
        // After Body Visitor (right)
        if (isSimilar(symbol, new Negotiator[]{Negotiator.FOR_SEQUENCE})) {
            return new Triplet<Object, Object, Object>(left, middle, right);

        } return null;
    }

    @Override
    public Object visitArityN(Block expression, Context context, MethodData currMethod, HashMap<String, byte[]> bytecodes) {
        Context nextContext;
        if (expression.getSymbol().equalsName("BLOCK")) nextContext = context;
        else if (expression.getSymbol().equalsName("BODY_SHADOW")) {
            nextContext = new Context(context, true);
            ProcedureType procType = (ProcedureType) context.getLast().getType();
            procType.addArgsToContext(nextContext);
            nextContext.add("|RETURN|", new VariableData(procType.getReturnType(), null, "SYSTEM"));
        }
        else nextContext = new Context(context, false);

        FieldsContainer field = new FieldsContainer();
        ArgumentsContainer args = new ArgumentsContainer();

        Object result = null;
        for (Expression child : expression.getChildren()) {
            result = child.accept(this, nextContext, currMethod, bytecodes);
            if (expression.getSymbol().equalsName("ARGUMENT")) {
                args.add(result);
            }
            else if (expression.getSymbol().equalsName("FIELD")) {
                if (result instanceof Variable vResult) {
                    if (!field.add(vResult)) throw new RuntimeException("Already declared field "+vResult.getIdentifier());
                } else throw new RuntimeException("Congratulation, You found the flag !!!");
            }
        }
        nextContext.intersect(context);
        if (expression.getSymbol().equalsName("ARGUMENT")) return args;
        if (expression.getSymbol().equalsName("FIELD")) return field;
        return result;
    }
}

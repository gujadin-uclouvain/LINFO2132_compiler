package compiler.CodeGeneration;

public final class BytecodeEnvironmentLoader extends ClassLoader {
    static { registerAsParallelCapable(); }

    public static final BytecodeEnvironmentLoader INSTANCE = new BytecodeEnvironmentLoader();

    public Class<?> defineClass(String binaryName, byte[] bytecode) {
        return defineClass(binaryName, bytecode, 0, bytecode.length);
    }
}

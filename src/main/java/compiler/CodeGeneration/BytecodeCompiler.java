package compiler.CodeGeneration;

import compiler.CodeGeneration.Tools.MethodData;
import compiler.Parser.Structure.Expression;

import compiler.Tools.Context.Context;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import static org.objectweb.asm.Opcodes.*;

public class BytecodeCompiler extends ASMTools {
    private final Context context = new Context();
    private byte[] globalBytecode = null;
    private final HashMap<String, byte[]> includes = new HashMap<>();

    private void generateBytecodeBuiltIn(ClassWriter creator) {
        addBuiltInProcedureInContext(context);
        BytecodeBuiltIn generator = new BytecodeBuiltIn();
        generator.generateNotFunction(creator);
        generator.generateGetCharFunction(creator);
        generator.generateGetLengthFunction(creator);
        generator.generateFloorFunction(creator);
        generator.generateReadFunction(creator);
        generator.generateWriteFunction(creator);
    }

    private void generateBytecodeTree(ClassWriter creator, MethodVisitor mainMethod, Expression rootExpr) {
        BytecodeGenerator generator = new BytecodeGenerator(creator);
        rootExpr.accept(generator, context, new MethodData(mainMethod, 0), this.includes);
    }

    public void generate(Expression rootExpr) {
        if (this.globalBytecode != null) return;
        ClassWriter creator = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        // public class <GlobalEnvironment> extends Objects {}
        creator.visit(V1_8, ACC_PUBLIC, this.globalEnv, null, objectClass(), null);

        // Generate BuiltIn Methods Bytecode
        generateBytecodeBuiltIn(creator);

        // public static void main(String[] args) {}
        String descriptor = createDescriptor(voidType(), stringArrayType());
        MethodVisitor mainMethod = createMethod(creator, "main", descriptor, ACC_PUBLIC | ACC_STATIC);

        // Generate Tree Program Bytecode
        generateBytecodeTree(creator, mainMethod, rootExpr);

        // Visit one more time the main method to add RETURN and #stacks / #locals
        mainMethod.visitCode();
        mainMethod.visitInsn(RETURN);
        mainMethod.visitMaxs(-1, -1);
        mainMethod.visitEnd();

        creator.visitEnd();
        globalBytecode = creator.toByteArray();
    }

    public void generateCompiledFiles() throws IOException {
        File outputDir = new File("./output");
        boolean isCreated = outputDir.mkdir();

        try (FileOutputStream fos = new FileOutputStream("./output/"+globalEnv+".class")) {
            fos.write(globalBytecode);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (Map.Entry<String, byte[]> bytecode : includes.entrySet()) {
            try (FileOutputStream fos = new FileOutputStream("./output/"+bytecode.getKey()+".class")) {
                fos.write(bytecode.getValue());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void test() {
        BytecodeEnvironmentLoader loader = new BytecodeEnvironmentLoader();

        for (Map.Entry<String, byte[]> bytecode : this.includes.entrySet().stream().filter(stringEntry -> !stringEntry.getKey().equals(globalEnv)).toList()) {
            loader.defineClass(bytecode.getKey(), bytecode.getValue());
        }
        Class<?> mainEnv = loader.defineClass(globalEnv, globalBytecode);
        try {
            mainEnv.getMethod("main", String[].class).invoke(null, (Object) new String[0]);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}

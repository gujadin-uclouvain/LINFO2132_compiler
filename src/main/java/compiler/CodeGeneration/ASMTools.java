package compiler.CodeGeneration;

import compiler.CodeGeneration.Tools.MethodData;
import compiler.Semantic.Checker;
import compiler.Tools.Context.Structure.ArgumentsContainer;
import compiler.Tools.Context.Structure.Variable;
import compiler.Tools.Context.Structure.VariableData;
import compiler.Tools.Type.ArrayType;
import compiler.Tools.Type.BasicType;
import compiler.Tools.Type.ProcedureType;
import compiler.Tools.Type.RecordType;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import java.util.Arrays;

import static org.objectweb.asm.Opcodes.*;

public class ASMTools extends Checker {
    public final String globalEnv = "GlobalEnvironment";
    int nbrTempVariable = 0;

    private final BasicType genericType = new BasicType(objectClass());
    private final BasicType integerType = new BasicType(integerTypeID);
    private final BasicType realType    = new BasicType(realTypeID);
    private final BasicType booleanType = new BasicType(booleanTypeID);
    private final BasicType stringType  = new BasicType(stringTypeID);
    private final BasicType voidType    = new BasicType(voidTypeID);

    private final ArrayType integerArrayType    = new ArrayType(integerTypeID);
    private final ArrayType realArrayType       = new ArrayType(realTypeID);
    private final ArrayType booleanArrayType    = new ArrayType(booleanTypeID);
    private final ArrayType stringArrayType     = new ArrayType(stringTypeID);

    int localCurrID = 1;

    // ------------------------------------------------------------------ //

    String objectClass() { return "java/lang/Object"; }
    String integerClass() { return "java/lang/Integer"; }
    String realClass() { return "java/lang/Float"; }
    String booleanClass() { return "java/lang/Boolean"; }
    String stringClass() { return "java/lang/String"; }
    String systemClass() { return "java/lang/System"; }
    String printClass() { return "java/io/PrintStream"; }
    String inputClass() { return "java/io/InputStream"; }

    BasicType genericType() { return genericType; }
    BasicType integerType() { return integerType; }
    BasicType realType()    { return realType; }
    BasicType booleanType() { return booleanType; }
    BasicType stringType()  { return stringType; }
    BasicType voidType()    { return voidType; }
    RecordType recordType(String className) { return new RecordType(className); }

    ArrayType integerArrayType()    { return integerArrayType; }
    ArrayType realArrayType()       { return realArrayType; }
    ArrayType booleanArrayType()    { return booleanArrayType; }
    ArrayType stringArrayType()     { return stringArrayType; }
    ArrayType recordArrayType(String className) { return new ArrayType(className); }


    String refType(String className) { return 'L'+className+';'; }

    String getType(BasicType type) { return getType(type, true); }
    String getType(BasicType type, boolean usePrimitiveType) {
        if (type instanceof ArrayType aType) { return getType(aType); }
        if (type instanceof RecordType rType) { return getType(rType); }
        return getType(type.getTypeID(), usePrimitiveType);
    }
    String getType(ArrayType type) {
        return '['+getType(type.getTypeID());
    }
    String getType(RecordType type) {
        return refType(type.getTypeID());
    }
    String getType(String type) { return getType(type, true); }
    String getType(String type, boolean usePrimitiveType) {
        if (type.equals(integerTypeID)) return usePrimitiveType ? "I" : refType(integerClass());
        if (type.equals(realTypeID)) return usePrimitiveType ? "F" : refType(realClass());
        if (type.equals(booleanTypeID)) return usePrimitiveType ? "Z" : refType(booleanClass());
        if (type.equals(stringTypeID)) return refType(stringClass());
        if (type.equals(voidTypeID)) return "V";
        return refType(type);
//        throw new RuntimeException("The current type '"+type+"' is unknown");
    }

    String getClass(BasicType type) { return getClass(type.getTypeID()); }
    String getClass(String type) {
        if (type.equals(integerTypeID)) return integerClass();
        if (type.equals(realTypeID))    return realClass();
        if (type.equals(booleanTypeID)) return booleanClass();
        if (type.equals(stringTypeID))  return stringClass();
        throw new RuntimeException("The current type '"+type+"' is unknown");
    }

    // ------------------------------------------------------------------ //

    MethodVisitor createMethod(ClassWriter env, String name, String descriptor, int access) {
        return env.visitMethod(access, name, descriptor, null, null);
    }

    String createDescriptor(BasicType returnType, BasicType... argsType) {
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        for (BasicType argType : argsType) {
            builder.append(getType(argType));
        }
        builder.append(")");
        builder.append(getType(returnType));
        return builder.toString();
    }

    String createCallableDescriptor(BasicType returnType, Variable[] vars) {
        BasicType[] args = Arrays.stream(vars).map(var -> var.getData().getType()).toArray(BasicType[]::new);
        return createDescriptor(returnType, args);
    }

    byte[] createClass(String className, Variable[] fields) {
        ClassWriter newCreator = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        newCreator.visit(V1_8, ACC_PUBLIC, className, null, objectClass(), null);
        for (Variable field : fields) {
            newCreator.visitField(ACC_PUBLIC, field.getIdentifier(), getType(field.getData().getType()), null, null).visitEnd();
        }
        createDefaultConstructor(newCreator, className, fields);
        newCreator.visitEnd();
        return newCreator.toByteArray();
    }

    private void createDefaultConstructor(ClassWriter env, String className, Variable[] fields) {
        // Create default constructor
        String descriptor = createCallableDescriptor(voidType(), fields);
        MethodVisitor constructor = env.visitMethod(ACC_PUBLIC, "<init>", descriptor, null, null);
        constructor.visitCode();
        constructor.visitVarInsn(ALOAD, 0);
        constructor.visitMethodInsn(INVOKESPECIAL, objectClass(), "<init>", createDescriptor(voidType()), false);
        for (Variable field : fields) {
            constructor.visitVarInsn(ALOAD, 0);
            loadVariable(constructor, field, 1);
            constructor.visitFieldInsn(PUTFIELD, className, field.getIdentifier(), getType(field.getData().getType()));
        }
        constructor.visitInsn(RETURN);
        constructor.visitMaxs(-1, -1);
        constructor.visitEnd();
    }

    // ------------------------------------------------------------------ //

    private void cmpInt(MethodVisitor method, int opcode) {
        Label notlabel = new Label();
        method.visitJumpInsn(opcode, notlabel);
        method.visitInsn(ICONST_0);
        Label endLabel = new Label();
        method.visitJumpInsn(GOTO, endLabel);
        method.visitLabel(notlabel);
        method.visitInsn(ICONST_1);
        method.visitLabel(endLabel);
    }

    public BasicType applyOperationInt(MethodVisitor method, String tokenOperation) {
        switch (tokenOperation) {
            case "+"  -> method.visitInsn(IADD);
            case "-"  -> method.visitInsn(ISUB);
            case "*"  -> method.visitInsn(IMUL);
            case "/"  -> method.visitInsn(IDIV);
            case "%"  -> method.visitInsn(IREM);
            case "==" -> cmpInt(method, IF_ICMPEQ);
            case "<>" -> cmpInt(method, IF_ICMPNE);
            case "<=" -> cmpInt(method, IF_ICMPLE);
            case "<"  -> cmpInt(method, IF_ICMPLT);
            case ">=" -> cmpInt(method, IF_ICMPGE);
            case ">"  -> cmpInt(method, IF_ICMPGT);
        }
        return integerType();
    }
    public BasicType applyOperationReal(MethodVisitor method, String tokenOperation) {
        int operation = 0;
        switch (tokenOperation) {
            case "+"  -> { method.visitInsn(FADD); return realType(); }
            case "-"  -> { method.visitInsn(FSUB); return realType(); }
            case "*"  -> { method.visitInsn(FMUL); return realType(); }
            case "/"  -> { method.visitInsn(FDIV); return realType(); }
            case "%"  -> { method.visitInsn(FREM); return realType(); }
            case "==" -> { method.visitInsn(FCMPG); operation = IFEQ; }
            case "<>" -> { method.visitInsn(FCMPG); operation = IFNE; }
            case "<=" -> { method.visitInsn(FCMPG); operation = IFLE; }
            case "<"  -> { method.visitInsn(FCMPG); operation = IFLT; }
            case ">=" -> { method.visitInsn(FCMPG); operation = IFGE; }
            case ">"  -> { method.visitInsn(FCMPG); operation = IFGT; }
        }
        if (operation != 0) cmpInt(method, operation);
        return integerType();
    }

    public BasicType applyOperationBool(MethodVisitor method, String tokenOperation) {
        switch (tokenOperation) {
            case "and" -> method.visitInsn(IAND);
            case "or"  -> method.visitInsn(IOR);
            case "=="  -> cmpInt(method, IF_ICMPEQ);
            case "<>"  -> cmpInt(method, IF_ICMPNE);
        }
        return integerType();
    }

    // ------------------------------------------------------------------ //

    VariableData initNewVariable(MethodData method, BasicType type) {
        int stackID = method.incLocalStackID();
        return new VariableData(type, stackID, null);
    }

    int getReturnOpcode(BasicType type) { return getReturnOpcode(type, true); }
    int getReturnOpcode(BasicType type, boolean usePrimitiveType) {
        if (!usePrimitiveType) return ARETURN;
        if (type.is(integerTypeID) || type.is(booleanTypeID)) return IRETURN;
        if (type.is(realTypeID)) return FRETURN;
        return ARETURN;
//        throw new RuntimeException("The current type '"+type+"' has no LOAD value");
    }

    int getLoadOpcode(BasicType type) { return getLoadOpcode(type, true); }
    int getLoadOpcode(BasicType type, boolean usePrimitiveType) {
        if (!usePrimitiveType) return ALOAD;
        if (type.is(integerTypeID) || type.is(booleanTypeID)) return ILOAD;
        if (type.is(realTypeID)) return FLOAD;
        return ALOAD;
//        throw new RuntimeException("The current type '"+type+"' has no LOAD value");
    }

    void loadValue(MethodVisitor method, Object value) {
        method.visitCode();
        method.visitLdcInsn(value);
        method.visitEnd();
    }

    public void loadVariable(MethodVisitor method, Variable variable) { // Push a Value of a Local into the Stack
        loadVariable(method, variable, 0);
    }
    public void loadVariable(MethodVisitor method, Variable variable, int offset) { // Push a Value of a Local into the Stack
        method.visitCode();
        if (!(variable.getData().getContent() instanceof Integer localID)) throw new RuntimeException("The content of a Variable Class must be an Integer");
        method.visitVarInsn(getLoadOpcode(variable.getData().getType()), localID + offset);
        method.visitEnd();
    }

    int getStoreOpcode(BasicType type) { return getStoreOpcode(type, true); }
    int getStoreOpcode(BasicType type, boolean usePrimitiveType) {
        if (!usePrimitiveType) return ASTORE;
        if (type.is(integerTypeID) || type.is(booleanTypeID)) return ISTORE;
        if (type.is(realTypeID)) return FSTORE;
        return ASTORE;
//        throw new RuntimeException("The current type '"+type+"' has no STORE value");
    }

    public void storeInVariable(MethodVisitor method, Variable variable) {
        storeInVariable(method, variable, 0);
    }
    public void storeInVariable(MethodVisitor method, Variable variable, int offset) { // Push a Stack Value into a Local
        method.visitCode();
        if (!(variable.getData().getContent() instanceof Integer localID)) throw new RuntimeException("The content of a Variable Class must be an Integer");
        method.visitVarInsn(getStoreOpcode(variable.getData().getType()), localID + offset);
        method.visitEnd();
    }

    public void returnLastInStack(MethodVisitor method, BasicType type) {
        method.visitCode();
        method.visitInsn(getReturnOpcode(type));
        method.visitEnd();
    }

    private void loadArguments(MethodVisitor method, ArgumentsContainer args) {
        for (Object arg : args.getContent()) {
            if (arg instanceof Variable vArg) {
                if (!(vArg.getData().getType() instanceof RecordType)) {
                    loadVariable(method, vArg);
                }
            } else {
                loadValue(method, arg);
            }
        }
    }

    public void invokeMethod(MethodVisitor method, Variable proc, ArgumentsContainer args) {
        if ((proc.getData().getType() instanceof ProcedureType procType)) {
            loadArguments(method, args);
            method.visitCode();
            method.visitMethodInsn(INVOKESTATIC, globalEnv, proc.getIdentifier(),
                    createDescriptor(procType.getReturnType(), procType.getArgsType()), false);
            method.visitEnd();

        } else throw new RuntimeException("A PROCEDURE must contain a ProcedureType");
    }

    public void invokeClass(MethodVisitor method, Variable record, ArgumentsContainer args) {
        if (record.getData().getType() instanceof RecordType recType) {
            method.visitCode();
            method.visitTypeInsn(NEW, recType.getTypeID());
            method.visitInsn(DUP);
            method.visitEnd();

            loadArguments(method, args);

            method.visitCode();
            method.visitMethodInsn(INVOKESPECIAL, recType.getTypeID(), "<init>",
                    createCallableDescriptor(voidType(), recType.getAll()), false
            );
            method.visitEnd();

        } else throw new RuntimeException("A RECORD must contain a RecordType");
    }

    // ------------------------------------------------------------------ //

    public void loadVisitedElemInStack(MethodVisitor method, Object elem) {
        if (elem instanceof Variable var) {
            loadVariable(method, var);
            if (var.getIdentifier().equals("|TEMP|")) { --nbrTempVariable; }
        } else loadValue(method, elem);
    }
}

package compiler.CodeGeneration.Tools;

import org.objectweb.asm.MethodVisitor;

public class MethodData {
    private final MethodVisitor method;
    private final int initLocalStackID;
    private int localStackID;

    public MethodData(MethodVisitor method, int initLocalStackID) {
        this.method = method;
        this.initLocalStackID = initLocalStackID;
        this.localStackID = initLocalStackID;
    }

    public MethodVisitor getMethod() { return method; }
    public int getLocalStackID() { return localStackID; }

    public int incLocalStackID() { return this.localStackID++; }
    public void resetLocalStackID() { this.localStackID = initLocalStackID; }
}

package compiler.CodeGeneration;

import compiler.Tools.Type.BasicType;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.*;

class BytecodeBuiltIn extends ASMTools {
    void generateNotFunction(ClassWriter globalEnv) {
        // static boolean not(boolean) {}
        String descriptor = createDescriptor(booleanType(), booleanType());
        MethodVisitor method = createMethod(globalEnv, "not", descriptor, ACC_STATIC);
        method.visitCode();
        // Load the first argument (an integer) in the stack
        method.visitVarInsn(ILOAD, 0);
        // Apply negation on the first elem in the stack
        method.visitInsn(INEG);
        // Return the first elem in the stack
        method.visitInsn(IRETURN);
        // End of the method
        method.visitEnd();
        // No elem in the stack & No locals used
        method.visitMaxs(-1, -1);
    }

    void generateGetCharFunction(ClassWriter globalEnv) {
        // static String chr(int) {}
        String intToStrDesc = createDescriptor(stringType(), integerType());
        MethodVisitor method = createMethod(globalEnv, "chr", intToStrDesc, ACC_STATIC);
        method.visitCode();
        // Load the first argument in the stack
        method.visitVarInsn(ILOAD, 0);
        // Call String.valueOf(int);
        method.visitMethodInsn(INVOKESTATIC, stringClass(), "valueOf", intToStrDesc, false);
        // Return the first elem (an object reference) in the stack
        method.visitInsn(ARETURN);
        // End of the method
        method.visitEnd();
        // No elem in the stack & No locals used
        method.visitMaxs(-1, -1);
    }

    void generateGetLengthFunction(ClassWriter globalEnv) {
        // static int len(String|Array) {}
        generateGetLengthStringFunction(globalEnv);
        generateGetLengthArrayFunction(globalEnv);
    }

    private void generateGetLengthStringFunction(ClassWriter globalEnv) {
        // static int len(String) {}
        String descriptor = createDescriptor(integerType(), stringType());
        MethodVisitor method = createMethod(globalEnv, "len", descriptor, ACC_STATIC);
        method.visitCode();
        // Load the first argument in the stack
        method.visitVarInsn(ALOAD, 0);
        // Call string.length();
        method.visitMethodInsn(INVOKEVIRTUAL, stringClass(), "length", createDescriptor(integerType()), false);
        method.visitInsn(IRETURN);
        method.visitEnd();
        method.visitMaxs(-1, -1);
    }

    private void generateGetLengthArrayFunction(ClassWriter globalEnv) {
        // static int len(Array) {}
        for (BasicType type : new BasicType[]{integerArrayType(), realArrayType(), booleanArrayType(), recordArrayType(objectClass())}) {
            String descriptor = createDescriptor(integerType(), type);
            MethodVisitor method = createMethod(globalEnv, "len", descriptor, ACC_STATIC);
            method.visitCode();
            // Load the first argument in the stack
            method.visitVarInsn(ALOAD, 0);
            // Get the length of the array
            method.visitInsn(ARRAYLENGTH);
            method.visitInsn(IRETURN);
            method.visitEnd();
            method.visitMaxs(-1, -1);
        }
    }

    void generateFloorFunction(ClassWriter globalEnv) {
        // static int floor(real) {}
        String descriptor = createDescriptor(integerType(), realType());
        MethodVisitor method = createMethod(globalEnv, "floor", descriptor, ACC_STATIC);
        method.visitCode();
        // Load the first argument in the stack
        method.visitVarInsn(FLOAD, 0);
        // Convert the float into an integer
        method.visitInsn(F2I);
        // Return the first elem (an integer) in the stack
        method.visitInsn(IRETURN);
        // End of the code
        method.visitEnd();
        // No elem in the stack & No locals used
        method.visitMaxs(-1, -1);
    }

    void generateReadFunction(ClassWriter globalEnv) {
        // $ => Type of the argument
        // static $ read$(void) {}
        String scannerClass = "java/util/Scanner";
        for (BasicType type : new BasicType[]{integerType(), realType(), stringType()}) {
            String descriptor = createDescriptor(type);
            String methodName = "read" + (type.is(integerTypeID) ? "Int" :
                                          type.is(realTypeID) ? "Real" :
                                          type.is(stringTypeID) ? "String" : "");
            MethodVisitor method = createMethod(globalEnv, methodName, descriptor, ACC_STATIC);
            method.visitCode();
            // Create a new instance of Scanner
            method.visitTypeInsn(NEW, scannerClass);
            // Duplicate the reference to the Scanner instance
            method.visitInsn(DUP);
            // Load System.out
            method.visitFieldInsn(GETSTATIC, systemClass(), "in", getType(inputClass()));
            // new Scanner(System.in);
            method.visitMethodInsn(INVOKESPECIAL, scannerClass, "<init>",
                    createDescriptor(voidType(), new BasicType(inputClass())), false);
            // Store the reference to the Scanner instance in local variable 0 (scanner)
            method.visitVarInsn(ASTORE, 0);

            // Load the reference to the Scanner instance onto the stack
            method.visitVarInsn(ALOAD, 0);
            // scanner.nextInt(); OR scanner.nextFloat(); OR scanner.nextLine();
            method.visitMethodInsn(INVOKEVIRTUAL, scannerClass,
                    (type.is(integerTypeID) ? "nextInt" : type.is(realTypeID) ? "nextFloat" : type.is(stringTypeID) ? "nextLine" : ""),
                    createDescriptor(type), false
            );

            method.visitInsn(getReturnOpcode(type));
            method.visitEnd();
            method.visitMaxs(-1, -1);
        }
    }

    private void generateNbrWriteFunction(ClassWriter globalEnv) {
        // $ => Type of the argument
        // static void write$($) {}
        BasicType[] basicTypes = new BasicType[]{integerType(), realType(), booleanType()};
        for (BasicType type : basicTypes) {
            String descriptor = createDescriptor(voidType(), type);
            String methodName = "write" + (type.is(integerTypeID) ? "Int" :
                                           type.is(booleanTypeID) ? "Bool" :
                                           type.is(realTypeID)    ? "Real" : "");
            MethodVisitor method = createMethod(globalEnv, methodName, descriptor, ACC_STATIC);

            method.visitCode();
            BasicType internalType = type.getTypeID().equals(booleanTypeID) ? integerType() : type;
            String internalDescriptor = createDescriptor(voidType(), internalType);
            // Load System.out
            method.visitFieldInsn(GETSTATIC, systemClass(), "out", getType(printClass()));
            // Load first arg in the stack
            method.visitVarInsn(getLoadOpcode(internalType), 0);
            // System.out.println(TYPE);
            method.visitMethodInsn(INVOKEVIRTUAL, printClass(), "print", internalDescriptor, false);
            method.visitInsn(RETURN);
            method.visitEnd();
            method.visitMaxs(-1, -1);
        }
    }

    void generateWriteFunction(ClassWriter globalEnv) {
        // $ => Type of the argument
        // static void write[ln](String) {}
        for (String variation : new String[]{"", "ln"}) {
            String descriptor = createDescriptor(voidType(), stringType());
            String methodName = "write" + variation;
            MethodVisitor method = createMethod(globalEnv, methodName, descriptor, ACC_STATIC);
            method.visitCode();
            // Load System.out
            method.visitFieldInsn(GETSTATIC, systemClass(), "out", getType(printClass()));
            // Load the first arg in the stack (s)
            method.visitVarInsn(getLoadOpcode(stringType()), 0);

            // System.out.println(String); OR System.out.print(String);
            method.visitMethodInsn(INVOKEVIRTUAL, printClass(), (variation.equals("ln")) ? "println" : "print",
                    descriptor, false
            );

            method.visitInsn(RETURN);
            method.visitEnd();
            method.visitMaxs(-1, -1);
        }
        generateNbrWriteFunction(globalEnv);
    }
}

import compiler.Lexer.Symbols.Symbol;
import compiler.Lexer.Symbols.SymbolType;
import compiler.Parser.Exception.ParserException;
import compiler.Parser.Structure.*;
import compiler.Tools.Test.TestToolbox;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class TestParser {

    @Test
    public void easyEquationParsing() throws Exception {
        String input = "6/3-1;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                        new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "6", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE))),
                        new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void mediumEquationParsing() throws Exception {
        String input = "3*4+5/4-46+3*100;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                        new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                                new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE)),
                                                new Literal(new Symbol("INTEGER_VALUE", "4", SymbolType.VALUE))),
                                        new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE)),
                                                new Literal(new Symbol("INTEGER_VALUE", "4", SymbolType.VALUE))
                                        )
                                ),
                                new Literal(new Symbol("INTEGER_VALUE", "46", SymbolType.VALUE))
                        ),
                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "100", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easyParenthesisEquationParsing() throws Exception {
        String input = "6/(3-1);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                        new Literal(new Symbol("INTEGER_VALUE", "6", SymbolType.VALUE)),
                        new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hardParenthesisEquationParsing() throws Exception {
        String input = "((6)/(((3-1))));";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                        new Literal(new Symbol("INTEGER_VALUE", "6", SymbolType.VALUE)),
                        new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void medium1ParenthesisEquationParsing() throws Exception {
        String input = "(8/78)*(9-8);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                        new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "8", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "78", SymbolType.VALUE))
                        ),
                        new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "9", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "8", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void medium2ParenthesisEquationParsing() throws Exception {
        String input = "(8/78)*9-(80-1);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                                        new Literal(new Symbol("INTEGER_VALUE", "8", SymbolType.VALUE)),
                                        new Literal(new Symbol("INTEGER_VALUE", "78", SymbolType.VALUE))
                                ),
                                new Literal(new Symbol("INTEGER_VALUE", "9", SymbolType.VALUE))),
                        new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "80", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easyLogicalParsing() throws Exception {
        String input = "true and false; true or false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("AND", "and", SymbolType.LOGICAL),
                        new Literal(new Symbol("BOOLEAN_VALUE", "true", SymbolType.VALUE)),
                        new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                ),
                new Binary(new Symbol("OR", "or", SymbolType.LOGICAL),
                        new Literal(new Symbol("BOOLEAN_VALUE", "true", SymbolType.VALUE)),
                        new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void medium1LogicalParsing() throws Exception {
        String input = "true and false or false and false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("OR", "or", SymbolType.LOGICAL),
                        new Binary(new Symbol("AND", "and", SymbolType.LOGICAL),
                                new Literal(new Symbol("BOOLEAN_VALUE", "true", SymbolType.VALUE)),
                                new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                        ),
                        new Binary(new Symbol("AND", "and", SymbolType.LOGICAL),
                                new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE)),
                                new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void medium2LogicalParsing() throws Exception {
        String input = "true or false and false or false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("OR", "or", SymbolType.LOGICAL),
                        new Binary(new Symbol("OR", "or", SymbolType.LOGICAL),
                                new Literal(new Symbol("BOOLEAN_VALUE", "true", SymbolType.VALUE)),
                                new Binary(new Symbol("AND", "and", SymbolType.LOGICAL),
                                        new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE)),
                                        new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                                )
                        ),
                        new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hard1LogicalParsing() throws Exception {
        String input = "true or false and true and false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("OR", "or", SymbolType.LOGICAL),
                        new Literal(new Symbol("BOOLEAN_VALUE", "true", SymbolType.VALUE)),
                        new Binary(new Symbol("AND", "and", SymbolType.LOGICAL),
                                new Binary(new Symbol("AND", "and", SymbolType.LOGICAL),
                                        new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE)),
                                        new Literal(new Symbol("BOOLEAN_VALUE", "true", SymbolType.VALUE))
                                ),
                                new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hard2LogicalParsing() throws Exception {
        String input = "true and false and false or false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("OR", "or", SymbolType.LOGICAL),
                        new Binary(new Symbol("AND", "and", SymbolType.LOGICAL),
                                new Binary(new Symbol("AND", "and", SymbolType.LOGICAL),
                                        new Literal(new Symbol("BOOLEAN_VALUE", "true", SymbolType.VALUE)),
                                        new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                                ),
                                new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                        ),
                        new Literal(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE))
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easyDoubleEquationParsing() throws Exception {
        String input = "6/(3-1);\n 6/3-1;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                        new Literal(new Symbol("INTEGER_VALUE", "6", SymbolType.VALUE)),
                        new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                        )
                ),
                new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                        new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "6", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE))
                        ),
                        new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hardMultipleParsing() throws Exception {
        String input = "6/(3-1);\n 6/3-1; x=5*8; y=8/78*9-(80-1);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                        new Literal(new Symbol("INTEGER_VALUE", "6", SymbolType.VALUE)),
                        new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                        )
                ),
                new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                        new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "6", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE))
                        ),
                        new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                ),
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER)),
                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "8", SymbolType.VALUE))
                        )
                ),
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER)),
                        new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                                new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                        new Binary(new Symbol("DIVIDE", "/", SymbolType.OPERATOR),
                                                new Literal(new Symbol("INTEGER_VALUE", "8", SymbolType.VALUE)),
                                                new Literal(new Symbol("INTEGER_VALUE", "78", SymbolType.VALUE))
                                        ),
                                        new Literal(new Symbol("INTEGER_VALUE", "9", SymbolType.VALUE))),
                                new Binary(new Symbol("MINUS", "-", SymbolType.OPERATOR),
                                        new Literal(new Symbol("INTEGER_VALUE", "80", SymbolType.VALUE)),
                                        new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void simpleParsing() throws Exception {
        String input = "var x int = 2;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                new Unary(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER),
                                    new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                )
                        ),
                        new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void simpleNegativeParsing() throws Exception {
        String input = "var x int = -2;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                new Unary(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                )
                        ),
                        new Literal(new Symbol("INTEGER_VALUE", "-2", SymbolType.VALUE))
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void simpleArrayParsing() throws Exception {
        String input = "var c int[];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                        new Unary(new Symbol("IDENTIFIER", "c", SymbolType.IDENTIFIER),
                                new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void simpleComparatorParsing() throws Exception {
        String input = "var x bool = 2 <> 3;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                new Unary(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("BOOLEAN", "bool", SymbolType.TYPE))
                                )
                        ),
                        new Binary(new Symbol("DIFFERENT", "<>", SymbolType.COMPARATOR),
                                new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE)),
                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void operationParsing() throws Exception {
        String input = "const identifier_name real = 3.2*5.0;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Unary(new Symbol("CONST", "const", SymbolType.DECLARATOR),
                                new Unary(new Symbol("IDENTIFIER", "identifier_name", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("REAL", "real", SymbolType.TYPE))
                                )
                        ),
                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                new Literal(new Symbol("REAL_VALUE", "3.2", SymbolType.VALUE)),
                                new Literal(new Symbol("REAL_VALUE", "5.0", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void returnParsing() throws Exception {
        String input = "var x int = 2; return x%15;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                new Unary(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                )
                        ),
                        new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                ),
                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                        new Binary(new Symbol("MODULO", "%", SymbolType.OPERATOR),
                                new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER)),
                                new Literal(new Symbol("INTEGER_VALUE", "15", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void simpleArrayIndexParsing() throws Exception {
        String input = "return p[0];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                new Literal(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void mediumArrayIndexParsing() throws Exception {
        String input = "return p[0] + p[1] * p[0] + p[1];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE))
                                        ),
                                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                                new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                        new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                        new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                                ),
                                                new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                        new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                        new Literal(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE))
                                                )
                                        )
                                ),
                                new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                        new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                        new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void simpleRecordArgumentParsing() throws Exception {
        String input = "return p.x;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                        new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void mediumRecordArgumentParsing() throws Exception {
        String input = "return p[0].x;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                        new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                        new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                        new Literal(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE))
                                ),
                                new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hardRecordArgumentParsing() throws Exception {
        String input = "return p[0].x+p[1].x * p[0].y+p[1].y;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                        new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                                new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                        new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                        new Literal(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE))),
                                                new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER))
                                        ),
                                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                                new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                                        ),
                                                        new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER))
                                                ),
                                                new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                                new Literal(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE))
                                                        ),
                                                        new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER))
                                                )
                                        )
                                ),
                                new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))),
                                        new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easyWhileLoopParsing() throws Exception {
        String input = """
                        while z <= 5 {
                            z = z+1;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("WHILE", "while", SymbolType.KEYWORD),
                        new Binary(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR),
                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))),
                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easy2WhileLoopParsing() throws Exception {
        String input = """
                        while z <= 5 {
                            z = z+1;
                            w = z+2;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("WHILE", "while", SymbolType.KEYWORD),
                        new Binary(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR),
                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                        ),
                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                        )
                                ),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Literal(new Symbol("IDENTIFIER", "w", SymbolType.IDENTIFIER)),
                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void mediumWhileLoopParsing() throws Exception {
        String input = """
                        while z <= 5 {
                            z = z+1;
                        }
                        return z;
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("WHILE", "while", SymbolType.KEYWORD),
                        new Binary(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR),
                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                        ),
                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                        )
                                )
                        )
                ),
                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                        new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER))
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hardWhileLoopParsing() throws Exception {
        String input = """
                        while z <= 5 {
                            z = z+1;
                            while y <= 5 {
                                while x <= 5 {
                                    x = x+1;
                                }
                                y = y+1;
                            }
                        }
                        return z;
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("WHILE", "while", SymbolType.KEYWORD),
                        new Binary(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR),
                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))),
                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                        )
                                ),
                                new Binary(new Symbol("WHILE", "while", SymbolType.KEYWORD),
                                        new Binary(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR),
                                                new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                                        ),
                                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                                new Binary(new Symbol("WHILE", "while", SymbolType.KEYWORD),
                                                        new Binary(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR),
                                                                new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER)),
                                                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))),
                                                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                                                        new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER)),
                                                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                                                new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER)),
                                                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                                                        )
                                                                )
                                                        )
                                                ),
                                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                                        new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER)),
                                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                                new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER)),
                                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                                        )
                                                )
                                        )
                                )
                        )
                ),
                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                        new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER))
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easy1ConditionParsing() throws Exception {
        String input = """
                        if z <= 5 {
                            const x int = 5;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("IF", "if", SymbolType.KEYWORD),
                        new Binary(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR),
                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                        ),
                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Unary(new Symbol("CONST", "const", SymbolType.DECLARATOR),
                                                new Unary(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER),
                                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                                )
                                        ),
                                        new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easy2ConditionParsing() throws Exception {
        String input = """
                        if z <= 5 {
                            const x int = 5;
                        } else {
                            const y real = 5.0;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Ternary(new Symbol("IF", "if", SymbolType.KEYWORD),
                        new Binary(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR),
                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                        ),
                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Unary(new Symbol("CONST", "const", SymbolType.DECLARATOR),
                                                new Unary(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER),
                                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                                )
                                        ),
                                        new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                                )
                        ),
                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Unary(new Symbol("CONST", "const", SymbolType.DECLARATOR),
                                                new Unary(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER),
                                                        new Literal(new Symbol("REAL", "real", SymbolType.TYPE))
                                                )
                                        ),
                                        new Literal(new Symbol("REAL_VALUE", "5.0", SymbolType.VALUE))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easy1ForLoopParsing() throws Exception {
        String input = """
                        var i int;
                        for i=1 to 100 by 2 {
                            z = z+1;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                        new Unary(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER),
                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                        )
                ),
                new Binary(new Symbol("FOR", "for", SymbolType.KEYWORD),
                        new Ternary(new Symbol("FOR_SEQUENCE", "", SymbolType.SPECIAL),
                            new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                    new Literal(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER)),
                                    new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                            ),
                            new Unary(new Symbol("TO", "to", SymbolType.KEYWORD),
                                    new Literal(new Symbol("INTEGER_VALUE", "100", SymbolType.VALUE))
                            ),
                            new Unary(new Symbol("BY", "by", SymbolType.KEYWORD),
                                    new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                            )
                        ),
                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                        )
                                ),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Literal(new Symbol("IDENTIFIER", "w", SymbolType.IDENTIFIER)),
                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easyRecordDefinitionParsing() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("RECORD", "record", SymbolType.KEYWORD),
                        new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER)),
                        new Block(new Symbol("FIELD", "", SymbolType.SPECIAL),
                                new Unary(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                ),
                                new Unary(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void mediumRecordDefinitionParsing() throws Exception {
        String input = """
                        record Person {
                            name string;
                            location Point;
                            history int[];
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("RECORD", "record", SymbolType.KEYWORD),
                        new Literal(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER)),
                        new Block(new Symbol("FIELD", "", SymbolType.SPECIAL),
                                new Unary(new Symbol("IDENTIFIER", "name", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("STRING", "string", SymbolType.TYPE))
                                ),
                                new Unary(new Symbol("IDENTIFIER", "location", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER))
                                ),
                                new Unary(new Symbol("IDENTIFIER", "history", SymbolType.IDENTIFIER),
                                        new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hardRecordDefinitionParsing() throws Exception {
        String input = """
                        record Person {
                            name string;
                            age int;
                            location Point;
                            historyLocations Point[];
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("RECORD", "record", SymbolType.KEYWORD),
                        new Literal(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER)),
                        new Block(new Symbol("FIELD", "", SymbolType.SPECIAL),
                                new Unary(new Symbol("IDENTIFIER", "name", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("STRING", "string", SymbolType.TYPE))
                                ),
                                new Unary(new Symbol("IDENTIFIER", "age", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                ),
                                new Unary(new Symbol("IDENTIFIER", "location", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER))
                                ),
                                new Unary(new Symbol("IDENTIFIER", "historyLocations", SymbolType.IDENTIFIER),
                                        new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easyProcedureDefinitionParsing() throws Exception {
        String input = """
                        proc square(v int) int {
                            return v*v;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Ternary(new Symbol("PROC", "proc", SymbolType.KEYWORD),
                        new Binary(new Symbol("PROC_INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "square", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Unary(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER),
                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                        )
                                )
                        ),
                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE)),
                        new Block(new Symbol("BODY_SHADOW", "", SymbolType.SPECIAL),
                                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void mediumProcedureDefinitionParsing() throws Exception {
        String input = """
                        proc fun(v int, z int) int {
                            var y int = v;
                            if z <= 5 {
                                const x int = 5;
                                y = y + x;
                            }
                            return y % v;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Ternary(new Symbol("PROC", "proc", SymbolType.KEYWORD),
                        new Binary(new Symbol("PROC_INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "fun", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Unary(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER),
                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                        ),
                                        new Unary(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER),
                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                        )
                                )
                        ),
                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE)),
                        new Block(new Symbol("BODY_SHADOW", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                                new Unary(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER),
                                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                                )
                                        ),
                                        new Literal(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER))
                                ),
                                new Binary(new Symbol("IF", "if", SymbolType.KEYWORD),
                                        new Binary(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR),
                                                new Literal(new Symbol("IDENTIFIER", "z", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                                        ),
                                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                                        new Unary(new Symbol("CONST", "const", SymbolType.DECLARATOR),
                                                                new Unary(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER),
                                                                    new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                                                )
                                                        ),
                                                        new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                                                ),
                                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                                        new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER)),
                                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                                new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER)),
                                                                new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER))
                                                        )
                                                )
                                        )
                                ),
                                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                                        new Binary(new Symbol("MODULO", "%", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hardProcedureDefinitionParsing() throws Exception {
        String input = """
                        proc test(p_all Point[], p Person, index int) Point {
                            return p_all[index];
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Ternary(new Symbol("PROC", "proc", SymbolType.KEYWORD),
                        new Binary(new Symbol("PROC_INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "test", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Unary(new Symbol("IDENTIFIER", "p_all", SymbolType.IDENTIFIER),
                                                new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                        new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER))
                                                )
                                        ),
                                        new Unary(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER),
                                                new Literal(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER))
                                        ),
                                        new Unary(new Symbol("IDENTIFIER", "index", SymbolType.IDENTIFIER),
                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                        )
                                )
                        ),
                        new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER)),
                        new Block(new Symbol("BODY_SHADOW", "", SymbolType.SPECIAL),
                                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                new Literal(new Symbol("IDENTIFIER", "p_all", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("IDENTIFIER", "index", SymbolType.IDENTIFIER))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hard2ProcedureDefinitionParsing() throws Exception {
        String input = """
                        proc square_array(v int) int[] {
                            val array_i int[] = int[](2);
                            array_i[0] = v;
                            array_i[1] = v*v;
                            return array_i;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Ternary(new Symbol("PROC", "proc", SymbolType.KEYWORD),
                        new Binary(new Symbol("PROC_INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "square_array", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Unary(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER),
                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                        )
                                )
                        ),
                        new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                        ),
                        new Block(new Symbol("BODY_SHADOW", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Unary(new Symbol("IMMUTABLE", "val", SymbolType.DECLARATOR),
                                                new Unary(new Symbol("IDENTIFIER", "array_i", SymbolType.IDENTIFIER),
                                                        new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                                        )
                                                )
                                        ),
                                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                                new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                                ),
                                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                                        new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                                                )
                                        )
                                ),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                new Literal(new Symbol("IDENTIFIER", "array_i", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE))
                                        ),
                                        new Literal(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER))
                                ),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                new Literal(new Symbol("IDENTIFIER", "array_i", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                        ),
                                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER))
                                        )
                                ),
                                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                                        new Literal(new Symbol("IDENTIFIER", "array_i", SymbolType.IDENTIFIER))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void simple1CallsParsing() throws Exception {
        String input = "return square(50);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "square", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Literal(new Symbol("INTEGER_VALUE", "50", SymbolType.VALUE))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void simple2CallsParsing() throws Exception {
        String input = "var c int[] = int[](5);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                new Unary(new Symbol("IDENTIFIER", "c", SymbolType.IDENTIFIER),
                                        new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                        )
                                )
                        ),
                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                ),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Literal(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE))
                                )
                        )
                )
        );

        assertEquals(toTest, expected);
    }

    @Test
    public void mediumCallsParsing() throws Exception {
        String input = "var d Person = Person(\"me\", p, a);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                new Unary(new Symbol("IDENTIFIER", "d", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER))
                                )
                        ),
                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Literal(new Symbol("STRING_VALUE", "\"me\"", SymbolType.VALUE)),
                                        new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                        new Literal(new Symbol("IDENTIFIER", "a", SymbolType.IDENTIFIER))
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hard1CallsParsing() throws Exception {
        String input = "var d Person = Person(\"me\", Point(3,7), (a*2));";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                new Unary(new Symbol("IDENTIFIER", "d", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER))
                                )
                        ),
                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Literal(new Symbol("STRING_VALUE", "\"me\"", SymbolType.VALUE)),
                                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                                new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER)),
                                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                                        new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE)),
                                                        new Literal(new Symbol("INTEGER_VALUE", "7", SymbolType.VALUE))
                                                )
                                        ),
                                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "a", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void hard2CallsParsing() throws Exception {
        String input = "var d Person = Person(\"me\", Point(3,7), int[](a*2));";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                new Unary(new Symbol("IDENTIFIER", "d", SymbolType.IDENTIFIER),
                                        new Literal(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER))
                                )
                        ),
                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Literal(new Symbol("STRING_VALUE", "\"me\"", SymbolType.VALUE)),
                                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                                new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER)),
                                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                                        new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE)),
                                                        new Literal(new Symbol("INTEGER_VALUE", "7", SymbolType.VALUE))
                                                )
                                        ),
                                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                                new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                                ),
                                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                                                new Literal(new Symbol("IDENTIFIER", "a", SymbolType.IDENTIFIER)),
                                                                new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                                                        )
                                                )
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void easyCallParserTest() throws IOException {
        String input = "not(true);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                        new Literal(new Symbol("IDENTIFIER", "not", SymbolType.IDENTIFIER)),
                        new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("BOOLEAN_VALUE", "true", SymbolType.VALUE))
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void badassParsing() throws Exception {
        String input = """
                // Deallocating arrays and records:
                // We assume that there is no garbage collector and that the created
                // arrays and records have to be manually deallocated when not needed anymore.
                //       var x Point = Point(3,5);
                //       delete x;
                // Accessing a deallocated array or record results in undefined behavior.
                // Deallocation is not deep. Before a record is deallocated, it is the duty
                // of the programmer to deallocate arrays or records referenced by that record.
                
                proc square(v int) int {
                    return v*v;
                }
                
                proc copyPoints(p Point[]) Point {
                     return Point(p[0].x+p[1].x, p[0].y+p[1].y);
                }
                
                proc main() void {
                    var value int = readInt();
                    writeln(square(value));
                    var i int;
                    for i=1 to 100 by 2 {
                        while value<>3 {
                            // ....
                        }
                    }
                    i = (i+2)*2;
                }
                """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Expression expected = new Block(
                new Ternary(new Symbol("PROC", "proc", SymbolType.KEYWORD),
                        new Binary(new Symbol("PROC_INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "square", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Unary(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER),
                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                        )
                                )
                        ),
                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE)),
                        new Block(new Symbol("BODY_SHADOW", "", SymbolType.SPECIAL),
                                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                                new Literal(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER)),
                                                new Literal(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER))
                                        )
                                )
                        )
                ),
                new Ternary(new Symbol("PROC", "proc", SymbolType.KEYWORD),
                        new Binary(new Symbol("PROC_INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "copyPoints", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                        new Unary(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER),
                                                new Unary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                        new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER))
                                                )
                                        )
                                )
                        ),
                        new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER)),
                        new Block(new Symbol("BODY_SHADOW", "", SymbolType.SPECIAL),
                                new Unary(new Symbol("RETURN", "return", SymbolType.KEYWORD),
                                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                                new Literal(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER)),
                                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                                new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                                                new Literal(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE))
                                                                        ),
                                                                        new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER))
                                                                ),
                                                                new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                                                        ),
                                                                        new Literal(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER))
                                                                )
                                                        ),
                                                        new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                                new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                                                new Literal(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE))
                                                                        ),
                                                                        new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER))
                                                                ),
                                                                new Binary(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER),
                                                                        new Binary(new Symbol("ARRAY", "[]", SymbolType.ARRAY),
                                                                                new Literal(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER)),
                                                                                new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                                                        ),
                                                                        new Literal(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER))
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                ),
                new Ternary(new Symbol("PROC", "proc", SymbolType.KEYWORD),
                        new Binary(new Symbol("PROC_INITIATOR", "", SymbolType.SPECIAL),
                                new Literal(new Symbol("IDENTIFIER", "main", SymbolType.IDENTIFIER)),
                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL))
                        ),
                        new Literal(new Symbol("VOID", "void", SymbolType.TYPE)),
                        new Block(new Symbol("BODY_SHADOW", "", SymbolType.SPECIAL),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                                new Unary(new Symbol("IDENTIFIER", "value", SymbolType.IDENTIFIER),
                                                        new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                                )
                                        ),
                                        new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                                new Literal(new Symbol("IDENTIFIER", "readInt", SymbolType.IDENTIFIER)),
                                                new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL))
                                        )
                                ),
                                new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                        new Literal(new Symbol("IDENTIFIER", "writeln", SymbolType.IDENTIFIER)),
                                        new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                                new Binary(new Symbol("INITIATOR", "", SymbolType.SPECIAL),
                                                        new Literal(new Symbol("IDENTIFIER", "square", SymbolType.IDENTIFIER)),
                                                        new Block(new Symbol("ARGUMENT", "", SymbolType.SPECIAL),
                                                                new Literal(new Symbol("IDENTIFIER", "value", SymbolType.IDENTIFIER))
                                                        )
                                                )
                                        )
                                ),
                                new Unary(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR),
                                        new Unary(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER),
                                                new Literal(new Symbol("INTEGER", "int", SymbolType.TYPE))
                                        )
                                ),
                                new Binary(new Symbol("FOR", "for", SymbolType.KEYWORD),
                                        new Ternary(new Symbol("FOR_SEQUENCE", "", SymbolType.SPECIAL),
                                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                                        new Literal(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER)),
                                                        new Literal(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE))
                                                ),
                                                new Unary(new Symbol("TO", "to", SymbolType.KEYWORD),
                                                        new Literal(new Symbol("INTEGER_VALUE", "100", SymbolType.VALUE))
                                                ),
                                                new Unary(new Symbol("BY", "by", SymbolType.KEYWORD),
                                                        new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                                                )
                                        ),
                                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL),
                                                new Binary(new Symbol("WHILE", "while", SymbolType.KEYWORD),
                                                        new Binary(new Symbol("DIFFERENT", "<>", SymbolType.COMPARATOR),
                                                                new Literal(new Symbol("IDENTIFIER", "value", SymbolType.IDENTIFIER)),
                                                                new Literal(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE))
                                                        ),
                                                        new Block(new Symbol("BODY", "", SymbolType.SPECIAL))
                                                )
                                        )
                                ),
                                new Binary(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT),
                                        new Literal(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER)),
                                        new Binary(new Symbol("TIMES", "*", SymbolType.OPERATOR),
                                                new Binary(new Symbol("PLUS", "+", SymbolType.OPERATOR),
                                                        new Literal(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER)),
                                                        new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                                                ),
                                                new Literal(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE))
                                        )
                                )
                        )
                )
        );
        assertEquals(toTest, expected);
    }

    @Test
    public void raiseCloseParenthesisParserTest() throws IOException {
        String input = "var x int = 50 * (12 /2;";
        try {
            TestToolbox.getParserOut(input, false);
            fail();
        } catch (ParserException e) {
            System.out.println(e.toString());
            assertTrue(true);
        }
    }

//    @Test
//    public void raiseOpenParenthesisParserTest() throws IOException {
//        String input = "(50)*2)*2;";
//        try {
//            TestToolbox.getParserOut(input, false);
//            fail();
//        } catch (ParserException e) {
//            System.out.println(e.toString());
//            assertTrue(true);
//        }
//    }

    @Test
    public void raiseAssignMissingParserTest() throws IOException {
        String input = "var x int 50;";
        try {
            TestToolbox.getParserOut(input, false);
            fail();
        } catch (ParserException e) {
            System.out.println(e.toString());
            assertTrue(true);
        }
    }

    @Test
    public void raiseTypeMissingParserTest() throws IOException {
        String input = "var x = 50;";
        try {
            TestToolbox.getParserOut(input, false);
            fail();
        } catch (ParserException e) {
            System.out.println(e.toString());
            assertTrue(true);
        }
    }

//    @Test
//    public void raiseDeclaratorMissingParserTest() throws IOException {
//        String input = "x int = 50;";
//        try {
//            TestToolbox.getParserOut(input, false);
//            fail();
//        } catch (ParserException e) {
//            System.out.println(e.toString());
//            assertTrue(true);
//        }
//    }

    @Test
    public void raiseAssignMisplacedParserTest() throws IOException {
        String input = "= var x int 50;";
        try {
            TestToolbox.getParserOut(input, false);
            fail();
        } catch (ParserException e) {
            System.out.println(e.toString());
            assertTrue(true);
        }
    }
}

import compiler.Lexer.Exception.LexerException;
import compiler.Lexer.Symbols.SymbolType;
import compiler.Tools.Test.TestToolbox;
import org.junit.Test;
import java.io.*;
import java.util.ArrayList;

import compiler.Lexer.Symbols.Symbol;

import static org.junit.Assert.*;


public class TestLexer {

    public void outputTest(ArrayList<Symbol> output, ArrayList<Symbol> expected){
        assertEquals(output.size(), expected.size());
        for(int i = 0; i<output.size(); i++){
            assertEquals(expected.get(i), output.get(i));
        }
    }

    @Test
    public void simpleAssign() throws IOException {
        String input = "var x int = 2;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER));
        expected.add(new Symbol( "INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);

    }

    @Test
    public void assignNoSpacing() throws IOException {
        String input = "const identifier_name real=3.2*5.0;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("CONST", "const", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "identifier_name", SymbolType.IDENTIFIER));
        expected.add(new Symbol("REAL", "real", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("REAL_VALUE", "3.2", SymbolType.VALUE));
        expected.add(new Symbol("TIMES", "*", SymbolType.OPERATOR));
        expected.add(new Symbol("REAL_VALUE", "5.0", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void assignRandomSpacing() throws IOException {
        String input = "val abc123 real=     \t3.0+ 42.17;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("IMMUTABLE", "val", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "abc123", SymbolType.IDENTIFIER));
        expected.add(new Symbol("REAL", "real", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("REAL_VALUE", "3.0", SymbolType.VALUE));
        expected.add(new Symbol("PLUS", "+", SymbolType.OPERATOR));
        expected.add(new Symbol("REAL_VALUE", "42.17", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }


    @Test
    public void multiLinesCode() throws IOException {
        String input = "val x int= 2;\nvar y real = 0.12;\n y=y*(x+1);\n const b bool   =     false;\n";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        // line 1
        expected.add(new Symbol("IMMUTABLE", "val", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        // line 2
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("REAL", "real", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("REAL_VALUE", "0.12", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        // line 3
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("TIMES", "*", SymbolType.OPERATOR));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER));
        expected.add(new Symbol("PLUS", "+", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        // line 4
        expected.add(new Symbol("CONST", "const", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("BOOLEAN", "bool", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("BOOLEAN_VALUE", "false", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void codeWithComment() throws IOException {
        String input = "var rabbit_number int =2;//represent the two first rabbit\nvar cage_number int=1;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        // line before comment
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "rabbit_number", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        // comment
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE", "represent the two first rabbit", SymbolType.COMMENT));
        // line after comment
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "cage_number", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void forLoop() throws IOException {
        String input = "var y int =2;\nfor i=0 to 10 by 1 {\n\ty = y*y;}"; // give 2^10
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("FOR", "for", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE));
        expected.add(new Symbol("TO", "to", SymbolType.KEYWORD));
        expected.add(new Symbol("INTEGER_VALUE", "10", SymbolType.VALUE));
        expected.add(new Symbol("BY", "by", SymbolType.KEYWORD));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("TIMES", "*", SymbolType.OPERATOR));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void whileLoop() throws IOException {
        String input = "var b int = 1;\nwhile b<>8 {\n\tb = b*2;}"; // stop when 3 loop are done
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("WHILE", "while", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("DIFFERENT", "<>", SymbolType.COMPARATOR));
        expected.add(new Symbol("INTEGER_VALUE", "8", SymbolType.VALUE));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("TIMES", "*", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void operatorTimes() throws IOException {
        String input = "var b int=2*21;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("TIMES", "*", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "21", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void operatorPLUS() throws IOException {
        String input = "var b int = 1+41;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("PLUS", "+", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "41", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void operatorMinusSimple() throws IOException {
        String input = "var b int = 43-1;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "43", SymbolType.VALUE));
        expected.add(new Symbol("MINUS", "-", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void operatorMinusHard() throws IOException {
        String input = "var b int = 41-(-1);";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "41", SymbolType.VALUE));
        expected.add(new Symbol("MINUS", "-", SymbolType.OPERATOR));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("MINUS", "-", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void operatorModulo() throws IOException {
        String input = "var b int = 7 %3;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "7", SymbolType.VALUE));
        expected.add(new Symbol("MODULO", "%", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void comparatorGT() throws IOException {
        String input = "var b bool = (7 > y);";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("BOOLEAN", "bool", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "7", SymbolType.VALUE));
        expected.add(new Symbol("GREATER", ">", SymbolType.COMPARATOR));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void comparatorLT() throws IOException {
        String input = "var b bool = (7 < y);";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("BOOLEAN", "bool", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "7", SymbolType.VALUE));
        expected.add(new Symbol("LESS", "<", SymbolType.COMPARATOR));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void comparatorGTE() throws IOException {
        String input = "var b bool = (7 >= y);";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("BOOLEAN", "bool", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "7", SymbolType.VALUE));
        expected.add(new Symbol("GREATER_EQUAL", ">=", SymbolType.COMPARATOR));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void comparatorLTE() throws IOException {
        String input = "var b bool = (7 <= y);";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "b", SymbolType.IDENTIFIER));
        expected.add(new Symbol("BOOLEAN", "bool", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "7", SymbolType.VALUE));
        expected.add(new Symbol("LESS_EQUAL", "<=", SymbolType.COMPARATOR));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void multiWordString() throws IOException {
        String input = "var str string = \"Hello World!\";"; // doesn't take the words after the first one ?
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "str", SymbolType.IDENTIFIER));
        expected.add(new Symbol("STRING", "string", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("STRING_VALUE", "\"Hello World!\"", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void funcDeclaration() throws IOException {
        String input = "proc doubleVal(arg int) int{\nreturn arg*2;\n}";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("PROC", "proc", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "doubleVal", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "arg", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("RETURN", "return", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "arg", SymbolType.IDENTIFIER));
        expected.add(new Symbol("TIMES", "*", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void funcCallTest() throws IOException {
        String input = "doubleVal(42) == 84;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("IDENTIFIER", "doubleVal", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "42", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("EQUAL", "==", SymbolType.COMPARATOR));
        expected.add(new Symbol("INTEGER_VALUE", "84", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void recordTest() throws IOException {
        String input = "record Point {\nx int;\ny int;\n}";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("RECORD", "record", SymbolType.TYPE));
        expected.add(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void innerRecord() throws IOException {
        String input = "record Person {\nname string;\n\tlocation Point;\n\thistory int[];\n}";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("RECORD", "record", SymbolType.TYPE));
        expected.add(new Symbol("IDENTIFIER", "Person", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "name", SymbolType.IDENTIFIER));
        expected.add(new Symbol("STRING", "string", SymbolType.TYPE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "location", SymbolType.IDENTIFIER));
        expected.add(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "history", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void arrayTest() throws IOException {
        String input = "var c int[] = int[](5);";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "c", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void arrayTestCommented() throws IOException {
        String input = "var c int[] = int[](5); // new array of length 5";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "c", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "5", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE", " new array of length 5", SymbolType.COMMENT));
        outputTest(toTest, expected);
    }

    @Test
    public void dotIdentifierWithDigitAtEndTestCommented() throws IOException {
        String input = "test1.sal5919ut5942.o85k92;";
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
        expected.add(new Symbol("IDENTIFIER", "test1", SymbolType.IDENTIFIER));
        expected.add(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "sal5919ut5942", SymbolType.IDENTIFIER));
        expected.add(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "o85k92", SymbolType.IDENTIFIER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void badassTest() throws IOException {
        String input = """
                // Deallocating arrays and records:
                // We assume that there is no garbage collector and that the created
                // arrays and records have to be manually deallocated when not needed anymore.
                //       var x Point = Point(3,5);
                //       delete x;
                // Accessing a deallocated array or record results in undefined behavior.
                // Deallocation is not deep. Before a record is deallocated, it is the duty
                // of the programmer to deallocate arrays or records referenced by that record.
                
                proc square(v int) int {
                    return v*v;
                }
                
                proc copyPoints(p Point[]) Point {
                     return Point(p[0].x+p[1].x, p[0].y+p[1].y);
                }
                
                proc main() void {
                    var value int = readInt();
                    writeln(square(value));
                    var i int;
                    for i=1 to 100 by 2 {
                        while value<>3 {
                            // ....
                        }
                    }
                    i = (i+2)*2;
                }
                """;
        ArrayList<Symbol> toTest = TestToolbox.getLexerOut(input, false);
        ArrayList<Symbol> expected = new ArrayList<>();
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE", " Deallocating arrays and records:", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE",
//                " We assume that there is no garbage collector and that the created",
//                SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE",
//                " arrays and records have to be manually deallocated when not needed anymore.",
//                SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE", "       var x Point = Point(3,5);", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE", "       delete x;", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE",
//                " Accessing a deallocated array or record results in undefined behavior.",
//                SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE",
//                " Deallocation is not deep. Before a record is deallocated, it is the duty",
//                SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE",
//                " of the programmer to deallocate arrays or records referenced by that record.",
//                SymbolType.COMMENT));
        expected.add(new Symbol("PROC", "proc", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "square", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("RETURN", "return", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER));
        expected.add(new Symbol("TIMES", "*", SymbolType.OPERATOR));
        expected.add(new Symbol("IDENTIFIER", "v", SymbolType.IDENTIFIER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));

        expected.add(new Symbol("PROC", "proc", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "copyPoints", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER));
        expected.add(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("RETURN", "return", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "Point", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER));
        expected.add(new Symbol("PLUS", "+", SymbolType.OPERATOR));
        expected.add(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "x", SymbolType.IDENTIFIER));
        expected.add(new Symbol("COLON", ",", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "0", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("PLUS", "+", SymbolType.OPERATOR));
        expected.add(new Symbol("IDENTIFIER", "p", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_BRACKET", "[", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_BRACKET", "]", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("DOT", ".", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "y", SymbolType.IDENTIFIER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));

        expected.add(new Symbol("PROC", "proc", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "main", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("VOID", "void", SymbolType.TYPE));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "value", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("IDENTIFIER", "readInt", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "writeln", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "square", SymbolType.IDENTIFIER));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "value", SymbolType.IDENTIFIER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("MUTABLE", "var", SymbolType.DECLARATOR));
        expected.add(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER));
        expected.add(new Symbol("INTEGER", "int", SymbolType.TYPE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("FOR", "for", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("INTEGER_VALUE", "1", SymbolType.VALUE));
        expected.add(new Symbol("TO", "to", SymbolType.KEYWORD));
        expected.add(new Symbol("INTEGER_VALUE", "100", SymbolType.VALUE));
        expected.add(new Symbol("BY", "by", SymbolType.KEYWORD));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("WHILE", "while", SymbolType.KEYWORD));
        expected.add(new Symbol("IDENTIFIER", "value", SymbolType.IDENTIFIER));
        expected.add(new Symbol("DIFFERENT", "<>", SymbolType.COMPARATOR));
        expected.add(new Symbol("INTEGER_VALUE", "3", SymbolType.VALUE));
        expected.add(new Symbol("OPEN_BRACE", "{", SymbolType.SINGLE_CHARACTER));
//        expected.add(new Symbol("COMMENT_ONE", "//", SymbolType.COMMENT));
//        expected.add(new Symbol("COMMENT_VALUE", " ....", SymbolType.COMMENT));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER));
        expected.add(new Symbol("ASSIGN", "=", SymbolType.ASSIGNMENT));
        expected.add(new Symbol("OPEN_PARENTHESIS", "(", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("IDENTIFIER", "i", SymbolType.IDENTIFIER));
        expected.add(new Symbol("PLUS", "+", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("CLOSE_PARENTHESIS", ")", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("TIMES", "*", SymbolType.OPERATOR));
        expected.add(new Symbol("INTEGER_VALUE", "2", SymbolType.VALUE));
        expected.add(new Symbol("SEMICOLON", ";", SymbolType.SINGLE_CHARACTER));
        expected.add(new Symbol("CLOSE_BRACE", "}", SymbolType.SINGLE_CHARACTER));
        outputTest(toTest, expected);
    }

    @Test
    public void raiseExceptionValueTest() throws IOException {
        String input = "1v*9";
        try {
            TestToolbox.getLexerOut(input, false);
            fail();
        } catch (LexerException e) {
            assertEquals(e.getMessage(), "The token '1v' is not a valid token");
        }
    }

    @Test
    public void raiseDotsAndUnknownExceptionTest() throws IOException {
        String input = "p857482.t91828 / 98547 | test;";
        try {
            ArrayList<Symbol> t = TestToolbox.getLexerOut(input, false);
            fail();
        } catch (LexerException e) {
            assertEquals(e.getMessage(), "The token '|' is not a valid token");
        }
    }
}
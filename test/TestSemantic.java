import compiler.Tools.Test.TestToolbox;
import compiler.Parser.Structure.*;
import compiler.Semantic.Semantic;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestSemantic {
    @Test
    public void easyEquationSemantic() throws Exception {
        String input = "6/3-1;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void mediumEquationSemantic() throws Exception {
        String input = "3*4+5/4-46+3*100;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyParenthesisEquationSemantic() throws Exception {
        String input = "6/(3-1);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void hardParenthesisEquationSemantic() throws Exception {
        String input = "((6)/(((3-1))));";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void medium1ParenthesisEquationSemantic() throws Exception {
        String input = "(8/78)*(9-8);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void medium2ParenthesisEquationSemantic() throws Exception {
        String input = "(8/78)*9-(80-1);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyLogicalSemantic() throws Exception {
        String input = "true and false; true or false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void medium1LogicalSemantic() throws Exception {
        String input = "true and false or false and false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void medium2LogicalSemantic() throws Exception {
        String input = "true or false and false or false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void hard1LogicalSemantic() throws Exception {
        String input = "true or false and true and false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void hard2LogicalSemantic() throws Exception {
        String input = "true and false and false or false;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyDoubleEquationSemantic() throws Exception {
        String input = "6/(3-1);\n 6/3-1;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void hardMultipleSemantic() throws Exception {
        String input = "var y int = 6/(3-1);\n var x real = 6.5/8.8-1; x=5.5*8; y=8/78*9-(80-1);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void simpleSemantic() throws Exception {
        String input = "var x int = 2;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void simpleBadSemantic() throws Exception {
        String input = "var x int = \"test\";";
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void simpleStoreContextSemantic() throws Exception {
        String input = "var x int; x = 2;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void simpleBadReassignIdentifierSemantic() throws Exception {
        String input = "const x int = 0; x = 2;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void simple2BadReassignIdentifierSemantic() throws Exception {
        String input = "val x string = \"hello\"; x = \"world\";";
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void simple2StoreContextSemantic() throws Exception {
        String input = "var x int; var y bool; x = 2;";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void complexSemantic() throws Exception {
        String input = """
                       var x int = 2;
                       var y int = 4;
                       var z int = x*y;
                       """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyIfSemantic() throws Exception {
        String input = """
                        var x int = 0;
                        var b bool = true;
                        if (b) {
                            x = 1;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easy2IfSemantic() throws Exception {
        String input = """
                        var x int = 0;
                        var b bool = true;
                        if (b) {
                            x = 1;
                            var y int = 2;
                            x = y + 2;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyBadScopeSemantic() throws Exception {
        String input = """
                        var x int = 0;
                        var b bool = true;
                        if (b) {
                            x = 1;
                            y = 2;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easy2BadScopeSemantic() throws Exception {
        String input = """
                        var x int = 0;
                        var b bool = true;
                        if (b) {
                            var x int = 2;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void mediumIfSemantic() throws Exception {
        String input = """
                        var x int;
                        var b bool = true;
                        x = 0;
                        if (b) {
                            x = 1;
                            var y int = 2;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyWhileSemantic() throws Exception {
        String input = """
                        var x int = 0;
                        while (x > 5) {
                            var inc int = 1;
                            x = x + inc;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyForSemantic() throws Exception {
        String input = """
                        var i int;
                        for i = 5 to 10 by 2 {
                            var inc int = 1;
                            i = i + inc;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyBadScopeForSemantic() throws Exception {
        String input = """
                        var i int;
                        for i = 5 to 10 by 2 {
                            var inc int = 1;
                            i = i + inc;
                        }
                        inc = 8;
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyBadSemanticForSemantic() throws Exception {
        String input = """
                        var i int;
                        for i = 5 to 10 by 2.5 {
                            var inc int = 1;
                            i = i + inc;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyArraySemantic() throws Exception {
        String input = "var c int[];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easy1ArraySemantic() throws Exception {
        String input = "var c int[] = int[](5);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void mediumArrayAccessSemantic() throws Exception {
        String input = "var c int[] = int[](5); var i int = c[2];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyBadAccessArraySemantic() throws Exception {
        String input = "var i int = c[0];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easy2BadAccessArraySemantic() throws Exception {
        String input = "var c int = 0; var i int = c[0];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyBadAccessIndexTypeArraySemantic() throws Exception {
        String input = "var c int = int[](5); var i int = c[0.8];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void mediumBadAccessTypeArraySemantic() throws Exception {
        String input = "var c real[] = int[](5); var i int = c[2];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyBadAccessIndexArraySemantic() throws Exception {
        String input = "var c int[] = int[](5); var i int = c[5];";
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyRecordDeclareSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyBadRecordDeclareSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                            x int;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyBadRecordNotDeclareSemantic() throws Exception {
        String input = "var p Point = Point(5, 10);";
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyRecordAssignSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        var p Point = Point(5, 10);
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void mediumRecordAssign1Semantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        var w int = 2;
                        var p Point = Point(w, 10);
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void mediumRecordAssign2Semantic() throws Exception {
        String input = """
                        record Point {
                            x int[];
                            y int;
                        }
                        var w int[] = int[](2);
                        var p Point = Point(w, 10);
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyBadRecordAssignArgsTypeSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        var p Point = Point(5, \"hello\");
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyBadRecordAssignArgsNbrMuchSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                        }
                        var p Point = Point(5, \"hello\");
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyBadRecordAssignArgsNbrLessSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        var p Point = Point(5);
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void hardRecordSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        record Person {
                            name string;
                            age int;
                            location Point;
                            history real[];
                        }
                        val hist real[] = real[](8);
                        val p Point = Point(5, 10);
                        var age int = 23;
                        var p1 Person = Person(\"me\", age, p, hist);
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void hard2RecordSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                        }
                        record Person {
                            history Point[];
                        }
                        var hist Point[] = Point[](1);
                        hist[0] = Point(1);
                        var p1 Person = Person(hist);
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void hardBadRecordSemantic() throws Exception {
        String input = """
                        record Person {
                            name string;
                            age int;
                            location Point;
                            historyLocations Point[];
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyRecordAccessSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                        }
                        var p Point = Point(5);
                        p.x = 10;
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void hardRecordAccessSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                        }
                        record Person {
                            point Point;
                        }
                        var p1 Point = Point(5);
                        var p Person = Person(p1);
                        p.point.x = 10;
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void veryHardRecordAccessSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                        }
                        record Person {
                            p Point;
                        }
                        var p1 Point = Point(5);
                        var p Person = Person(p1);
                        p.p.x = 10;
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void hardBadRecordAccessSemantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                        }
                        record Person {
                            point Point;
                        }
                        var p1 Point = Point(5);
                        var p Person = Person(p1);
                        p.point.x = \"hello\";
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyStructureFileSemantic() throws Exception {
        String input = """
                        const t int = 5;
                        const v int = 5;
                        record Point {
                            x int;
                            y int;
                        }
                        record Point2 {
                            x int;
                            y int;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyBadStructureFileSemantic() throws Exception {
        String input = """
                        const t int = 5;
                        record Point {
                            x int;
                            y int;
                        }
                        const v int = 5;
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyBadStructureFile2Semantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        const v int = 5;
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyBadStructureFile3Semantic() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        var v int = 5;
                        record Point2 {
                            x int;
                            y int;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        try {
            Semantic checker = new Semantic();
            assertFalse(checker.analyse(toTest));
        } catch (Exception e) {
            System.out.println(e);
            assertTrue(true);
        }
    }

    @Test
    public void easyStructureProcedureSemantic() throws Exception {
        String input = """
                        var i int = 0;
                        proc square(v int) int {
                            return 8;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

    @Test
    public void easyReturnStructureProcedureSemantic() throws Exception {
        String input = """
                        proc square(v int) int {
                            return v*v;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

//    @Test
//    public void easyBadReturnStructureProcedureSemantic() throws Exception {
//        String input = """
//                       var s string = "hello";
//                       proc square(v int) int {
//                           return s;
//                       }
//                       """;
//        Expression toTest = TestToolbox.getParserOut(input, false);
//        try {
//            Semantic checker = new Semantic();
//            assertFalse(checker.analyse(toTest));
//        } catch (Exception e) {
//            System.out.println(e);
//            assertTrue(true);
//        }
//    }

    @Test
    public void easyShadowProcedureSemantic() throws Exception {
        String input = """
                        var nbr int = 5;
                        proc square(v int) int {
                            var nbr int = 9;
                            return v*v;
                        }
                        """;
        Expression toTest = TestToolbox.getParserOut(input, false);
        Semantic checker = new Semantic();
        assertTrue(checker.analyse(toTest));
    }

//    @Test
//    public void badassSemantic() throws Exception {
//        String input = """
//                record Point {
//                    x int;
//                    y int;
//                }
//
//                record Person {
//                    name string;
//                    location Point;
//                    history int[];
//                }
//
//                proc square(v int) int {
//                    return v*v;
//                }
//
//                proc copyPoints(p Point[]) Point {
//                     return Point(p[0].x+p[1].x, p[0].y+p[1].y);
//                }
//
//                proc main() void {
//                    var value int = square(587);
//                    var i int;
//                    for i=1 to 100 by 2 {
//                        while value<>3 {
//                            // ....
//                        }
//                    }
//                    i = (i+2)*2;
//                }
//                """;
//        Expression toTest = TestToolbox.getParserOut(input, false);
//        Semantic checker = new Semantic();
//        assertTrue(checker.analyse(toTest));
//    }
}

import compiler.Tools.Test.TestToolbox;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestCodeGeneration {
    @Test
    public void assignValue() throws Exception {
        String input =  """
                        var i int = 2;
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void assignTwoValue() throws Exception {
        String input =  """
                        var i int = 2;
                        var j int = 3;
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void assignRealValue() throws Exception {
        String input =  """
                        var i real = 2.5;
                        var j real = 3.9;
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void assignMultipleValue() throws Exception {
        String input =  """
                        var i int = 2;
                        var j real = 3.5;
                        var s string = "welcome";
                        var b bool = true;
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void assignVariable() throws Exception {
        String input =  """
                        var i int = 2;
                        var j int = i;
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callWriteInt() throws Exception {
        String input =  """
                        writeInt(5);
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callWriteReal() throws Exception {
        String input =  """
                        writeReal(5.85);
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callWritelnString() throws Exception {
        String input =  """
                        writeln("Hello World !");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callWriteString() throws Exception {
        String input =  """
                        write("Hello ");
                        write("World !\n");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callWriteAll() throws Exception {
        String input =  """
                        write("const i int = ");
                        writeInt(52);
                        writeln(";");
                        write("var s string = ");
                        write("'Hello ");
                        write("World !';\n");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callWriteIntWithAssign() throws Exception {
        String input =  """
                        var i int = 52;
                        writeInt(i);
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callMultipleIntAssign() throws Exception {
        String input =  """
                        var i int = 2;
                        var j int = i;
                        var k int = i;
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callMultipleRealAssign() throws Exception {
        String input =  """
                        var i real = 2.2;
                        var j real = i;
                        var k real = i;
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callWriteRealWithMultipleAssign() throws Exception {
        String input =  """
                        var i real = 2.2;
                        var j real = 3.3;
                        var k real = 4.4;
                        writeReal(i);
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callHardWriteIntWithAssign() throws Exception {
        String input =  """
                        var i int = 8;
                        var j int = +85;
                        writeInt(i); writeln("");
                        var k int = -945;
                        var l int = 50;
                        writeInt(k); writeln("");
                        writeInt(j); writeln("");
                        writeInt(l); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callReadInt() throws Exception {
        // Cannot make tests with read functions (need to generate compiled class)
        String input =  """
                        var i int = readInt();
                        writeInt(i);
                        """;
        //TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callMediumMultipleWriteWithAssign() throws Exception {
        String input =  """
                        var i int = 2;
                        var j real = 3.5;
                        writeInt(i); writeln("");
                        writeReal(j); writeln("");
                        var s string = "welcome";
                        writeln(s);
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callHardMultipleWriteWithAssign() throws Exception {
        String input =  """
                        var i int = 2;
                        var j real = 3.5;
                        writeInt(i); writeln("");
                        var s string = "welcome";
                        writeln(s);
                        writeReal(j); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void computeIntAssign() throws Exception {
        String input =  """
                        var i int = 2+4;
                        writeInt(i); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void generateProcedure() throws Exception {
        String input = """
                        proc square(v int) int {
                            return v;
                        }
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void generateComplexProcedure() throws Exception {
        String input = """
                        proc complex(v int, t string, r real, v5 int) real {
                            return r;
                        }
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void generateMultipleProcedure() throws Exception {
        String input = """
                        proc complex(v int, t string, r real, v5 int) real {
                            return r;
                        }
                        proc square(v int) int {
                            return v;
                        }
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callSimpleProcedure() throws Exception {
        String input = """
                        proc simple(v int) int {
                            return v;
                        }
                        var i int = simple(5);
                        //writeInt(i); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callMediumProcedure() throws Exception {
        String input = """
                        proc simple(v int) int {
                            return v;
                        }
                        var i int = simple(5);
                        writeInt(i); writeln("");
                        
                        writeInt(simple(10)); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callComplexProcedure() throws Exception {
        String input = """
                        proc simple(v int) int {
                            return v;
                        }
                        proc complex(v int, t string, r real, v5 int) real {
                            return r;
                        }
                        var i int = simple(5);
                        
                        var r real = complex(i, "exotic butter", 0.5, i);
                        writeReal(r); writeln("");
                        writeInt(i); writeln("");
                        
                        writeReal(complex(i, "exotic butter", r, i)); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callBodyProcedure() throws Exception {
        String input = """
                        proc square(v int) int {
                            return v*v;
                        }
                        var i int = square(5);
                        writeInt(i); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void generateRecord() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callRecord() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        
                        var p Point = Point(2, 4);
                        p = Point(4, 5);
                        //writeln("callRecord");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callFieldRecord() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        
                        var p Point = Point(24, 4);
                        var x int = p.x;
                        var y int = p.y;
                        writeInt(x); writeln("");
                        writeInt(y); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void callField2Record() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        
                        var p Point = Point(26, 4);
                        writeInt(p.x + p.y); writeln("");
                        writeBool(p.x + p.y == 30); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void ModifyFieldRecord() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                        
                        var p Point = Point(26, 4);
                        p.x = p.x + 38 - p.x / 1;
                        writeInt(p.x + p.y); writeln("");
                        writeBool(p.x + p.y == 30); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void calculateVariablesInt() throws Exception {
        String input = """
                        var i1 int = 2;
                        var i2 int = 4;
                        var i3 int = 6;
                        var result int = (2+i1) * (i2+4) * (i3+i3) ;
                        writeInt(result); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void calculateVariablesReal() throws Exception {
        String input = """
                        var i1 real = 2.4;
                        var i2 real = 4.8;
                        var i3 real = 6.5;
                        var result real = i3 - i2 / i1 * i2 + i3 ;
                        writeReal(result); writeln("");
                        """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void calculateVariablesBool() throws Exception {
        String input = """
                       var i1 bool = true;
                       var i2 bool = false;
                       var i3 bool = true;
                       var result bool = (i3 and i1 and i1 and i1 and i3) and i2;
                       writeBool(result); writeln("");
                       """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void comparatorIntVariable() throws Exception {
        String input = """
                       var i1 int = 42;
                       var i2 int = 42;
                       var result bool = i1 == i2;
                       write("== : "); writeBool(result); writeln("");
                       result = i1 <> i2;
                       write("<> : "); writeBool(result); writeln("");
                       result = i1 < i2;
                       write("< : "); writeBool(result); writeln("");
                       result = i1 <= i2;
                       write("<= : "); writeBool(result); writeln("");
                       result = i1 > i2;
                       write("> : "); writeBool(result); writeln("");
                       result = i1 >= i2;
                       write(">= : "); writeBool(result); writeln("");
                       """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void comparatorRealVariable() throws Exception {
        String input = """
                       var i1 real = 42.45;
                       var i2 real = 42.11;
                       var result bool = i1 == i2;
                       write("== : "); writeBool(result); writeln("");
                       result = i1 <> i2;
                       write("<> : "); writeBool(result); writeln("");
                       result = i1 < i2;
                       write("< : "); writeBool(result); writeln("");
                       result = i1 <= i2;
                       write("<= : "); writeBool(result); writeln("");
                       result = i1 > i2;
                       write("> : "); writeBool(result); writeln("");
                       result = i1 >= i2;
                       write(">= : "); writeBool(result); writeln("");
                       """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void generateIfCondition() throws Exception {
        String input = """
                       var x int = 0;
                       var b bool = true;
                       if (b and true) {
                           writeInt(x); writeln("");
                       }
                       writeInt(x+5); writeln("");
                       """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void generateIfElseCondition() throws Exception {
        String input = """
                       var x int = 42;
                       if (58 >= x) {
                           writeInt(x); writeln("");
                       } else {
                           writeInt(x+5); writeln("");
                       }
                       """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void generateWhileCondition() throws Exception {
        String input = """
                        var x int = 0;
                        while (x <= 5) {
                            writeInt(x); writeln("");
                            x = x + 1;
                        }
                       """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void generateDoubleWhileCondition() throws Exception {
        String input = """
                        var x int = 0;
                        while (x < 5) {
                            writeInt(x); write(" ");
                            x = x + 1;
                        }
                        writeInt(x); write(" ");
                        writeln("\nSwap Loop");
                        while (x >= 0) {
                            writeInt(x); write(" ");
                            x = x - 1;
                        }
                        writeln("");
                       """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void generateForCondition() throws Exception {
        String input = """
                        var i int;
                        for i = 0 to 11 by 2 {
                            writeInt(i); writeln("");
                        }
                       """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void procVoidTest() throws Exception {
        String input = """
                       proc main() void {
                           writeln("test");
                           return;
                       }
                       main();
                       """;
        TestToolbox.testCompiledCode(input, false);
    }

    @Test
    public void badassTest() throws Exception {
        String input = """
                        record Point {
                            x int;
                            y int;
                        }
                                                
                        proc square(v int) int {
                            return v*v;
                        }
                                                
                        proc main() void {
                            // Record test
                            var value int = square(5);
                            var p Point = Point(0, value);
                            write("Point.x => "); writeInt(p.x); writeln("");
                            write("Point.y => "); writeInt(p.y); writeln("");
                            p.x = p.y;
                            write("Point.x => "); writeInt(p.x); writeln("");
                            write("Point.y => "); writeInt(p.y); writeln("");
                            
                            // Proc test
                            var i int;
                            var carre int;
                            for i=1 to 500 by 50 {
                                carre = square(i);
                                writeInt(carre); writeln("");
                            }
                            return;
                        }
                        main();
                        """;
        TestToolbox.testCompiledCode(input, false);
    }
}

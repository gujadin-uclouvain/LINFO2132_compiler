# ZIP
DIR_NAME = LINFO2132_compiler_ALLEGAERT_Hadrien_JADIN_Guillaume
ZIP_NAME = ./$(DIR_NAME).zip
MAIN_FILES = src test build.gradle.kts

zip:
	@mkdir ${DIR_NAME}
	@$(foreach file,${MAIN_FILES},cp -r ${file} ${DIR_NAME}/${file};)
	@git log --stat > ${DIR_NAME}/.gitlog.stat
	zip -r ${ZIP_NAME} ${DIR_NAME}
	@rm -r -d ${DIR_NAME}

.PHONY: zip